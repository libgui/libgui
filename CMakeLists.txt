cmake_minimum_required (VERSION 3.13)
project (cppgui VERSION 1.0.0 LANGUAGES CXX C)

# External configuration CMake scripts
set (CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/cmake")

option (GUI_BUILD_EXAMPLES "Build example applications [default: OFF]" OFF)

include (main)
include (utils)

#print_all_variables()
show_build_info_if_needed()
