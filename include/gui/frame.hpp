/*
    Name:        gui/frame.hpp
    Purpose:     Frame box interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_FRAME_HPP
#define GUI_FRAME_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class frame

    Frame box.
*/
class frame : public widget<T>
{
public:
//  No child widgets without parent.
    frame() = delete;
/**
    Constructor.
    @param parent The parent widget.
    @param label  The frame label.
*/
    frame( widget<T> *parent, const std::string &label = std::string() );

/** Destructor. */
    ~frame();
/**
    Sets the label to display inside the frame.
    @param label Text to display.
*/
    void set_label( const std::string &label );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/frame.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/frame.ipp"
#endif

#endif // GUI_FRAME_HPP
