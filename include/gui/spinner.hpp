/*
    Name:        gui/spinner.hpp
    Purpose:     GTK spinner interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/29
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_SPINNER_HPP
#define GUI_SPINNER_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class spinner

    Spinner.
*/
class spinner : public widget<T>
{
public:
//  No child widgets without parent.
    spinner() = delete;
/**
    Constructor.
    @param parent The parent widget.
*/
    spinner( widget<T> *parent );

/** Destructor. */
    ~spinner();

/** Starts the spinner animation. */
    void start();

/** Stops the spinner animation. */
    void stop();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/spinner.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/spinner.ipp"
#endif

#endif // GUI_SPINNER_HPP
