/*
    Name:        gui/window.hpp
    Purpose:     Window interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/20
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WINDOW_HPP
#define GUI_WINDOW_HPP

#include "gui/config.hpp"
#include "gui/toplevel.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class window

    Window.
*/
class window : public toplevel<T>
{
public:
/** Constructor. */
    window() : window( nullptr ) {}
/**
    Constructor.
    @param parent The parent window.
*/
    window( toplevel<T> *parent );

/** Virtual destructor. */
    virtual ~window();

    void show_resize_grip( bool show );

private:
    void do_set_size( int width, int height );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/window.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/window.ipp"
#endif

#endif // GUI_WINDOW_HPP
