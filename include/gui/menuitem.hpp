/*
    Name:        gui/menuitem.hpp
    Purpose:     Menu item interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/07
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_MENUITEM_HPP
#define GUI_MENUITEM_HPP

#include <string>
#include <boost/signals2/signal.hpp>
#include "gui/config.hpp"
#include "gui/menu.hpp"

namespace gui {

template <typename T>
class menu;

template <typename T = native_backend_t>
/**
    @class menuitem

    Menu item.
*/
class menuitem
{
public:
//  Don't create menuitems without a label.
    menuitem() = delete;
/**
    Constructor.
    @param label The item label.
*/
    menuitem(const std::string &label);

/** Destructor */
    ~menuitem();
/**
    Returns the menuitem label, including mnemonics.
    @return The menuitem label, including mnemonics.
*/
    std::string label() const { return m_label; }
/**
    Gets the menu item id.
    @return ID or gui::not_found.
*/
    int id() const;
/**
    Returns the native menuitem handle.
    @return The native menuitem handle.
*/
    native_menuitem_t native() const { return m_native; }

/** Selected signal. */
    boost::signals2::signal<void(menuitem<T> *)> sig_select;

protected:
    friend class menu<T>;
    native_menuitem_t m_native; // Native menuitem.
    std::string       m_label;  // Menu item label.
    menu<T>          *m_parent; // Parent (sub)menu.
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/menuitem.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/menuitem.ipp"
#endif

#endif // GUI_MENUITEM_HPP
