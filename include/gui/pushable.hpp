/*
    Name:        gui/pushable.hpp
    Purpose:     Pushable widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_PUSHABLE_HPP
#define GUI_PUSHABLE_HPP

#include <string>
#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class pushable

    Interface for button like widgets.
*/
class pushable : public widget<T>
{
public:
/** Virtual destructor. */
    virtual ~pushable() {};
/**
    Sets the button label.
    @param label The button's label.
*/
    void set_label( const std::string &label );
/**
    Sets the button image from an icon name.
    @param name The name of the icon to display.
*/
    void set_image( const std::string &name );

protected:
    pushable( widget<T> *parent ) : widget<T>( parent ) {}

private:
    pushable() = default;

    virtual void do_set_label( const std::string &label );
    virtual void do_set_image( const std::string &name );
};

} // namespace gui

#include "gui/common/pushable.ipp"

#if GUI_USE_GTK
    #include "gui/gtk/pushable.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/pushable.ipp"
#endif

#endif // GUI_PUSHABLE_HPP
