/*
    Name:        gui/range.hpp
    Purpose:     Range widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_RANGE_IPP
#define GUI_RANGE_IPP

#include "gui/widget.hpp"

namespace gui {

template<typename T>
/**
    @class range

    Range widget interface.
    Common interface for scrollbars and sliders.
*/
class range : public widget<T>
{
public:
/** Virtual destructor. */
    virtual ~range();
/**
    Returns the orientation of the range widget.
    @return The orientation of the range widget.
*/
    orientation_t orientation() const;
/**
    Returns the minimum bound of the range widget.
    @return The minimum bound of the range widget.
*/
    int min() const;
/**
    Returns the maximum bound of the range widget.
    @return The maximum bound of the range widget.
*/
    int max() const;
/**
    Sets the minimum bound for the range widget.
    @param min New minimum position value.
*/
    void set_min( int min );
/**
    Sets the maximum bound for the range widget.
    @param max New maximum position value.
*/
    void set_max( int max );
/**
    Sets the position value for the range widget.
    @param position New position.
*/
    void set_position( int position );
/**
    Returns the position value of the range widget.
    @return The position value of the range widget.
*/
    int position() const;

protected:
    range( widget<T> *parent, orientation_t orientation );

    orientation_t m_orientation;
    int           m_min;
    int           m_max;
    int           m_pos;

private:
    virtual void do_set_min( int min ) = 0;
    virtual void do_set_max( int max ) = 0;
    virtual void do_set_position( int position ) = 0;
};

} // namespace gui

#include "gui/common/range.ipp"

#endif // GUI_RANGE_IPP
