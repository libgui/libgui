// This file is not used (yet?) but here just for reference.

#include <algorithm>
#include <iterator>
#include <locale>
#include <string>
using namespace std;

namespace gui {

string narrow(const wstring& s)
{
    string out;
    out.reserve(s.size());
    transform(s.begin(), s.end(), back_inserter(out),
        [](wchar_t w) { return use_facet<ctype<wchar_t>>(locale()).narrow(w, '?'); }
    );
    return out;
}

wstring widen(const string& s)
{
    wstring out;
    out.reserve(s.size());
    transform(s.begin(), s.end(), back_inserter(out),
        [](char c) { return use_facet<ctype<wchar_t>>(locale()).widen(c); }
    );
    return out;
}

} // namespace gui
