/*
    Name:        gui/common/bookshell.ipp
    Purpose:     Common book based widgets implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_COMMON_BOOKSHELL_IPP
#define GUI_COMMON_BOOKSHELL_IPP

namespace gui {

template<typename T>
bookshell<T>::bookshell(widget<T> *parent)
:
widget<T>::widget(parent),
m_selection(not_found),
m_controller(nullptr)
{
}

template<typename T>
bookshell<T>::~bookshell()
{
    m_pages.clear();
}

template<typename T>
int bookshell<T>::append_page( widget<T> *page, const std::string& label )
{
    return insert_page( page, label, m_pages.size() );
}

template<typename T>
int bookshell<T>::prepend_page( widget<T> *page, const std::string& label )
{
    return insert_page( page, label, 0 );
}

template<typename T>
int bookshell<T>::insert_page( widget<T> *page, const std::string& label,
                               size_t index )
{
    if( index >= m_pages.size() )
    {
        m_pages.push_back( page_t( page, label ) );
    }
    else
    {
        m_pages.insert( m_pages.begin() + index, page_t( page, label ) );
    }

    return do_insert_page( page, label, index );
}

template<typename T>
size_t bookshell<T>::page_count() const
{
    return m_pages.size();
};

template<typename T>
int bookshell<T>::selection() const
{
    return m_selection;
}

template<typename T>
void bookshell<T>::set_selection( size_t index )
{
    if( (m_selection >= 0) && (m_selection < m_pages.size()) )
    {
        m_pages[m_selection].page->hide();
    }

    if( (index >= 0) && (index < m_pages.size()) )
    {
        m_selection = index;
    }
    else
    {
        m_selection = m_pages.size() - 1;
    }

    m_pages[m_selection].page->show();

    do_set_selection( index );
}

} // namespace gui

#endif // GUI_COMMON_BOOKSHELL_IPP
