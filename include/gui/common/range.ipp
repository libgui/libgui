/*
    Name:        gui/common/range.ipp
    Purpose:     Common range implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_COMMON_RANGE_IPP
#define GUI_COMMON_RANGE_IPP

namespace gui {

template<typename T>
range<T>::range( widget<T> *parent, orientation_t orientation )
:
widget<T>::widget(parent),
m_orientation( orientation ),
m_min(0),
m_max(0),
m_pos(0)
{
}

template<typename T>
range<T>::~range()
{
}

template<typename T>
orientation_t range<T>::orientation() const
{
    return m_orientation;
}

template<typename T>
int range<T>::min() const
{
    return m_min;
}

template<typename T>
int range<T>::max() const
{
    return m_max;
}

template<typename T>
void range<T>::set_min( int min )
{
    if( min > m_max )
        return;

    m_min = min;

    if( m_pos < m_min )
        m_pos = m_min;

    do_set_min( min );
}

template<typename T>
void range<T>::set_max( int max )
{
    if( max < m_min )
        return;

    m_max = max;

    if( m_pos > m_max )
        m_pos = m_max;

    do_set_max( max );
}

template<typename T>
void range<T>::set_position( int position )
{
    if( (position < m_min) || (position > m_max) )
        return;

    m_pos = position;
    do_set_position( position );
}

template<typename T>
int range<T>::position() const
{
    return m_pos;
}

} // namespace gui

#endif // GUI_COMMON_RANGE_IPP
