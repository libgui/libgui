/*
    Name:        gui/common/pushable.ipp
    Purpose:     Pushable widget common implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_COMMON_PUSHABLE_IPP
#define GUI_COMMON_PUSHABLE_IPP

namespace gui {

template<typename T>
void pushable<T>::set_label( const std::string &label )
{
    do_set_label( label );
}

template<typename T>
void pushable<T>::set_image( const std::string &name )
{
    do_set_image( name );
}

} // namespace gui

#endif // GUI_COMMON_PUSHABLE_IPP
