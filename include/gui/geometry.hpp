/*
    Name:        gui/geometry.hpp
    Purpose:     geometry types
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/14
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GEOMETRY_HPP
#define GUI_GEOMETRY_HPP

/** GUI namespace. */
namespace gui {

/** Widget orientation. */
enum orientation_t
{
    orientation_none = 0,                      /**< Unspecified orientation. */
    horizontal,                                /**< Horizontal orientation. */
    vertical,                                  /**< Vertical orientation. */
    both_orientations = horizontal | vertical, /**< All orientations. */
    orientation_mask  = both_orientations      /**< Orientation mask. */
};

/** Widget direction. */
enum direction_t
{
    direction_none, /**< Unspecified direction. */
    top,
    up      = top,
    bottom,
    down    = bottom,
    left,
    right
};

} // namespace gui

#endif // GUI_GEOMETRY_HPP
