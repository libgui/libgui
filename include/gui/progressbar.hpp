/*
    Name:        gui/progressbar.hpp
    Purpose:     Progress bar widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_PROGRESSBAR_HPP
#define GUI_PROGRESSBAR_HPP

#include "gui/geometry.hpp"
#include "gui/range.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class progressbar

    A widget to display the progress of a running operation.
*/
class progressbar : public range<T>
{
public:
//  No child widgets without parent.
    progressbar() = delete;
/**
    Constructor.
    @param parent      The parent widget.
    @param orientation The horizontal or vertical bar orientation.
*/
    progressbar( widget<T> *parent, orientation_t orientation = horizontal );

/** Destructor. */
    ~progressbar();

    void pulse();

private:
    void do_set_min( int min );
    void do_set_max( int max );
    void do_set_position( int position );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/progressbar.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/progressbar.ipp"
#endif

#endif // GUI_PROGRESSBAR_HPP
