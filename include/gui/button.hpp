/*
    Name:        gui/button.hpp
    Purpose:     Button widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_BUTTON_HPP
#define GUI_BUTTON_HPP

#include <string>
#include <boost/signals2/signal.hpp>
#include "gui/config.hpp"
#include "gui/pushable.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class button

    Push button.
*/
class button : public pushable<T>
{
public:
/** Constructor. */
    button() {}
/**
    Constructor.
    @param parent The parent widget.
*/
    button( widget<T> *parent );

/** Destructor. */
    ~button();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/button.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/button.ipp"
#endif

#endif // GUI_BUTTON_HPP
