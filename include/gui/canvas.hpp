/*
    Name:        gui/canvas.hpp
    Purpose:     Drawable widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/21
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_CANVAS_HPP
#define GUI_CANVAS_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class canvas

    Custom drawable widget.
*/
class canvas : public widget<T>
{
public:
//  No child widgets without parent.
    canvas() = delete;
/**
    Constructor.
    @param parent The parent widget.
*/
    canvas( widget<T> *parent );

/** Destructor. */
    ~canvas();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/canvas.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/canvas.ipp"
#endif

#endif // GUI_CANVAS_HPP
