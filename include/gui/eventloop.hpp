/*
    Name:        eventloop.hpp
    Purpose:     Event loop interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/23
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_EVENTLOOP_HPP
#define GUI_EVENTLOOP_HPP

#include "gui/config.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class eventloop

    Generic eventloop class:
    any application should be associated with at least one
    instance of this class to be able to process events.
*/
class eventloop
{
public:
    eventloop();          /**< Constructor. */
    virtual ~eventloop(); /**< Virtual destructor. */
    void exit();          /**< Exits the loop. */
    void run();           /**< Starts the loop. */
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/eventloop.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/eventloop.ipp"
#endif

#endif // GUI_EVENTLOOP_HPP
