/*
    Name:        gui/detail/platform.hpp
    Purpose:     platform definitions, see gui/config.hpp
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/14
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_DETAIL_PLATFORM_HPP
#define GUI_DETAIL_PLATFORM_HPP

#if defined(__MACH__) && defined(__APPLE__)
#   define GUI_HAVE_COCOA
#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#   define GUI_HAVE_WIN32
#endif

#endif // GUI_DETAIL_PLATFORM_HPP
