/*
    Name:        gui/application.hpp
    Purpose:     Application interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/20
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_APPLICATION_HPP
#define GUI_APPLICATION_HPP

#include <vector>
#include <functional>

#include <boost/signals2/signal.hpp>

#include "gui/config.hpp"
#include "gui/eventloop.hpp"
#include "gui/toplevel.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class application

    The main application.
*/
class application
{
public:
    typedef std::vector< toplevel<T>* > toplevels;
/**
    Constructor to use with main() arguments.
    @param argc Argument count.
    @param argv Arguments.
*/
    application(int argc, char** argv)
    :   m_argc(argc),
        m_argv(argv),
        m_loop(new eventloop<T>())
    {
        init();
    }

/** Constructor. */
    application() : application( 0, nullptr ) {}

/** Destructor, called by mainloop::exit(). */
    ~application();

/** Runs the mainloop. */
    void run();
/**
    Adds a window to the vector of known toplevel windows.
    @param window Window to add.
    @return @true if the window was added, @false otherwise.
*/
    bool add_window( toplevel<T> *window )
    {
        if( std::find( m_toplevels.begin(),
                       m_toplevels.end(), window ) == m_toplevels.end() )
        {
            m_toplevels.push_back( window );
            return true;
        }

        return false;
    }
/**
    Removes a specified window from the vector of known toplevel windows if found.
    If the window was the last one, stops the running loop.
    @param window Window to remove.
*/
    void remove_window( toplevel<T> *window )
    {
        typename toplevels::iterator it = std::find( m_toplevels.begin(),
                                                     m_toplevels.end(), window );
        if( it == m_toplevels.end() )
            return;

        m_toplevels.erase( it );

        if( m_loop && m_toplevels.empty() )
        {
            m_loop->exit();
            m_loop.reset();
        }
    }
/**
    Sets the main application window and adds it to the vector if new.
    @param window Window to set as main one.
*/
    void set_main_window( toplevel<T> *window )
    {
        if( !window )
            return;

        add_window( window );
        m_mainwindow = window;

        m_mainwindow->sig_destroy.disconnect( &application::on_destroy_window );

        m_mainwindow->sig_destroy.connect( std::bind(
                        &application::on_destroy_window, this, m_mainwindow ));
    }

private:
    void on_destroy_window( toplevel<T> *window )
    {
        this->remove_window( window );
    }

    void init();

    int           m_argc;
    char**        m_argv;
    toplevels     m_toplevels;

    toplevel<T>  *m_mainwindow;
    std::unique_ptr< eventloop<T> > m_loop;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/application.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/application.ipp"
#endif

#endif // GUI_APPLICATION_HPP
