/*
    Name:        gui/separator.hpp
    Purpose:     GTK separator interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/29
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_SEPARATOR_HPP
#define GUI_SEPARATOR_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class separator

    Separator.
*/
class separator : public widget<T>
{
public:
//  No child widgets without parent.
    separator() = delete;
/**
    Constructor.
    @param parent      The parent widget.
    @param orientation The separator orientation.
*/
    separator( widget<T> *parent, orientation_t orientation = horizontal );

/** Virtual destructor. */
    virtual ~separator();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/separator.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/separator.ipp"
#endif

#endif // GUI_SEPARATOR_HPP
