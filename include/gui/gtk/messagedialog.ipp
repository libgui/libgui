/*
    Name:        gui/gtk/messagedialog.ipp
    Purpose:     GTK message dialog implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/18
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_MESSAGEDIALOG_HPP
#define GUI_GTK_MESSAGEDIALOG_HPP

namespace gui {

template <typename T>
messagedialog<T>::messagedialog(toplevel<T> *parent,
                                const std::string &message,
                                const std::string &title,
                                int id,
                                msg_t msgtype)
:
dialog<T>::dialog(parent),
m_message(message),
m_title(title),
m_id(id),
m_msgtype(msgtype)
{
}

template <typename T>
messagedialog<T>::~messagedialog()
{
}

template <typename T>
int messagedialog<T>::show_modal()
{
    if( !this->m_parent )
        return id_none;

    GtkButtonsType btnsType = GTK_BUTTONS_NONE;

    if( m_id & id_ok )
    {
        btnsType = m_id & id_cancel ? GTK_BUTTONS_OK_CANCEL : GTK_BUTTONS_OK;
    }
    else if( m_id & id_cancel )
    {
        btnsType = GTK_BUTTONS_CANCEL;
    }
    else if( m_id & id_close )
    {
        btnsType = GTK_BUTTONS_CLOSE;
    }
    else if( m_id & (id_yes|id_no) )
    {
        btnsType = GTK_BUTTONS_YES_NO;
    }

    GtkMessageType msgType;

    switch( m_msgtype )
    {
    case msg_info:
        msgType = GTK_MESSAGE_INFO;
        break;
    case msg_warning:
        msgType = GTK_MESSAGE_WARNING;
        break;
    case msg_question:
        msgType = GTK_MESSAGE_QUESTION;
        break;
    case msg_error:
        msgType = GTK_MESSAGE_ERROR;
        break;
    case msg_other:
        msgType = GTK_MESSAGE_OTHER;
        break;
    default:
        msgType = GTK_MESSAGE_INFO;
    }

    this->m_native = gtk_message_dialog_new(GTK_WINDOW(this->m_parent->native()),
                                            GTK_DIALOG_MODAL,
                                            msgType, btnsType, m_message.c_str());
    if( !m_title.empty() )
        this->set_title( m_title );

    int response = gtk_dialog_run( GTK_DIALOG(this->m_native) );

    gtk_widget_destroy( this->m_native );

    switch( response )
    {
        case GTK_RESPONSE_DELETE_EVENT:
        case GTK_RESPONSE_NONE:   return id_none;
        case GTK_RESPONSE_OK:     return id_ok;
        case GTK_RESPONSE_CANCEL: return id_cancel;
        case GTK_RESPONSE_CLOSE:  return id_close;
        case GTK_RESPONSE_YES:    return id_yes;
        case GTK_RESPONSE_NO:     return id_no;
        case GTK_RESPONSE_APPLY:  return id_abort;
        case GTK_RESPONSE_HELP:   return id_retry;
        default:                  return id_none;
    }
}

} // namespace gui

#endif // GUI_GTK_MESSAGEDIALOG_HPP
