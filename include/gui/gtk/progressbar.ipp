/*
    Name:        gui/gtk/progressbar.ipp
    Purpose:     GTK progress bar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_PROGRESSBAR_IPP
#define GUI_GTK_PROGRESSBAR_IPP

namespace gui {

template<typename T>
progressbar<T>::progressbar( widget<T> *parent,
                             orientation_t orientation )
:
range<T>::range( parent, orientation )
{
    this->m_native = gtk_progress_bar_new();

    if( orientation == vertical )
    {
        gtk_orientable_set_orientation( GTK_ORIENTABLE(this->m_native),
                                        GTK_ORIENTATION_VERTICAL );

        gtk_progress_bar_set_inverted( GTK_PROGRESS_BAR(this->m_native), true );
    }
}

template<typename T>
progressbar<T>::~progressbar()
{
}

template<typename T>
void progressbar<T>::do_set_min( int /*min*/ )
{
    do_set_position( this->m_pos );
}

template<typename T>
void progressbar<T>::do_set_max( int /*max*/ )
{
    do_set_position( this->m_pos );
}

template<typename T>
void progressbar<T>::do_set_position( int /*position*/ )
{
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(this->m_native),
                                  this->m_max ?
                                ((double)this->m_pos)/this->m_max : 0.0);
}

template<typename T>
void progressbar<T>::pulse()
{
    
}

} // namespace gui

#endif // GUI_GTK_PROGRESSBAR_IPP
