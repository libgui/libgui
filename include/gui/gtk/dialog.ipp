/*
    Name:        gui/gtk/dialog.ipp
    Purpose:     GTK dialog implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/18
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_DIALOG_IPP
#define GUI_GTK_DIALOG_IPP

namespace gui {

template <typename T>
dialog<T>::dialog()
{
}

template <typename T>
dialog<T>::dialog( toplevel<T> *parent )
:
toplevel<T>::toplevel(parent)
{
}

template <typename T>
dialog<T>::~dialog()
{
}

} // namespace gui

#endif // GUI_GTK_DIALOG_IPP
