/*
    Name:        gui/gtk/application.ipp
    Purpose:     GTK application implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_APPLICATION_IPP
#define GUI_GTK_APPLICATION_IPP

#include <iostream>

namespace gui {

template <typename T>
void application<T>::init()
{
    gtk_init( &m_argc, &m_argv );
}

template <typename T>
application<T>::~application()
{
    if( m_loop )
        m_loop->exit();
}

template <typename T>
void application<T>::run()
{
    if( m_loop )
        m_loop->run();
}

} // namespace gui

#endif // GUI_GTK_APPLICATION_IPP
