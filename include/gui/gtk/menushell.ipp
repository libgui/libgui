/*
    Name:        gui/gtk/menushell.ipp
    Purpose:     GTK menushell implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/07
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_MENUSHELL_IPP
#define GUI_GTK_MENUSHELL_IPP

#include <iostream>

namespace gui {

template <typename T>
menushell<T>::menushell()
{
}

template <typename T>
menushell<T>::~menushell()
{
}

template <typename T>
void menushell<T>::push_back( menuitem<T> *item )
{
    gtk_menu_shell_append( GTK_MENU_SHELL(this->m_menu), item->native() );
}

template <typename T>
void menushell<T>::push_front( menuitem<T> *item )
{
    gtk_menu_shell_prepend( GTK_MENU_SHELL(this->m_menu), item->native() );
}

template <typename T>
void menushell<T>::insert( int position, menuitem<T> *item )
{
    gtk_menu_shell_insert( GTK_MENU_SHELL(this->m_menu), item->native(), position );
}

} // namespace gui

#endif // GUI_GTK_MENUSHELL_IPP
