/*
    Name:        gui/gtk/grid.ipp
    Purpose:     GTK grid implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/07
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_GRID_IPP
#define GUI_GTK_GRID_IPP

#include <iostream>

namespace gui {

template <typename T>
grid<T>::grid()
{
    this->m_native = gtk_grid_new();
}

template <typename T>
grid<T>::grid(widget<T> *parent)
:
widget<T>::widget(parent)
{
    this->m_native = gtk_grid_new();
}

template <typename T>
grid<T>::~grid()
{
}

template <typename T>
void grid<T>::attach(const widget<T> &child, int col, int row,
                     int colspan, int rowspan)
{
    gtk_grid_attach(GTK_GRID(this->m_native), child.native(), col, row, colspan, rowspan);
}

} // namespace gui

#endif // GUI_GTK_GRID_IPP
