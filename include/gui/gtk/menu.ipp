/*
    Name:        gui/gtk/menu.ipp
    Purpose:     GTK menu implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_MENU_IPP
#define GUI_GTK_MENU_IPP

namespace gui {

template <typename T>
menu<T>::menu()
{
    m_native = gtk_menu_new();
}

template <typename T>
menu<T>::~menu()
{
}

template <typename T>
void menu<T>::add_submenu( menu<T> *child, const std::string &label )
{
    GtkWidget *item = gtk_menu_item_new_with_mnemonic( label.c_str() );

    gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), child->native() );

    gtk_menu_shell_append( GTK_MENU_SHELL(m_native), item );
}

template <typename T>
void menu<T>::add_item( menuitem<T> *item )
{
    if( item )
        gtk_menu_shell_append( GTK_MENU_SHELL(m_native), item->native() );
}

template <typename T>
menuitem<T> * menu<T>::item_at( size_t index )
{
//  Not implemented
    return nullptr;
}

template <typename T>
menuitem<T> * menu<T>::item_by_id( int id )
{
//  Not implemented
    return nullptr;
}

} // namespace gui

#endif // GUI_GTK_MENU_IPP
