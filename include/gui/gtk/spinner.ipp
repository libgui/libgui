/*
    Name:        gui/gtk/spinner.ipp
    Purpose:     GTK spinner implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/29
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_SPINNER_IPP
#define GUI_GTK_SPINNER_IPP

namespace gui {

template <typename T>
spinner<T>::spinner( widget<T> *parent )
:
widget<T>::widget(parent)
{
    this->m_native = gtk_spinner_new();
}

template <typename T>
spinner<T>::~spinner()
{
}

template <typename T>
void spinner<T>::start()
{
    gtk_spinner_start( GTK_SPINNER(this->m_native) );
}

template <typename T>
void spinner<T>::stop()
{
    gtk_spinner_stop( GTK_SPINNER(this->m_native) );
}

} // namespace gui

#endif // GUI_GTK_SPINNER_IPP
