/*
    Name:        gui/gtk/checkbox.ipp
    Purpose:     GTK checkbox implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_CHECKBOX_IPP
#define GUI_GTK_CHECKBOX_IPP

namespace gui {

template<typename T>
checkbox<T>::checkbox( widget<T> *parent )
:
pushable<T>::pushable(parent)
{
    this->m_native =  gtk_check_button_new_with_mnemonic("");

    g_signal_connect( this->m_native, "clicked",
                      G_CALLBACK(gtk_callback_clicked_event), this );
}

template<typename T>
checkbox<T>::~checkbox()
{
}

} // namespace gui

#endif // GUI_GTK_CHECKBOX_IPP
