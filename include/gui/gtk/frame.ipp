/*
    Name:        gui/gtk/frame.ipp
    Purpose:     GTK frame implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_FRAME_IPP
#define GUI_GTK_FRAME_IPP

#include <iostream>

namespace gui {

template <typename T>
frame<T>::frame( widget<T> *parent, const std::string &label )
:
widget<T>::widget( parent )
{
    this->m_native = gtk_frame_new( label.c_str() );
}

template <typename T>
frame<T>::~frame()
{
}

template <typename T>
void frame<T>::set_label( const std::string &label )
{
    gtk_frame_set_label( this->m_native, label.c_str() );
}

} // namespace gui

#endif // GUI_GTK_FRAME_IPP
