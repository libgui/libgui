/*
    Name:        gui/gtk/statusbar.ipp
    Purpose:     GTK statusbar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_STATUSBAR_IPP
#define GUI_GTK_STATUSBAR_IPP

#include <iostream>

namespace gui {

template <typename T>
statusbar<T>::statusbar( widget<T> *parent )
:
widget<T>::widget(parent)
{
    this->m_native = gtk_statusbar_new();
}

template <typename T>
statusbar<T>::~statusbar()
{
}

template <typename T>
void statusbar<T>::set_text(const std::string &text)
{
// TODO
    int ctxid = gtk_statusbar_get_context_id(
                GTK_STATUSBAR(this->m_native), text.c_str() );

    int id = gtk_statusbar_push(
             GTK_STATUSBAR(this->m_native), ctxid, text.c_str() );
}

} // namespace gui

#endif // GUI_GTK_STATUSBAR_IPP
