/*
    Name:        gui/gtk/pushable.ipp
    Purpose:     GTK pushable widget implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_PUSHABLE_IPP
#define GUI_GTK_PUSHABLE_IPP

static gboolean
gtk_callback_clicked_event( GtkWidget */*widget*/,
                            gui::pushable<gui::gtk> *btn )
{
    btn->sig_select(btn);
    return true;
}

namespace gui {

template<typename T>
void pushable<T>::do_set_label( const std::string &label )
{
    gtk_button_set_label( GTK_BUTTON(this->m_native), label.c_str() ) ;
}

template<typename T>
void pushable<T>::do_set_image( const std::string &name )
{
    GtkWidget *image = gtk_image_new_from_icon_name( name.c_str(),
                                                    GTK_ICON_SIZE_BUTTON );
    gtk_button_set_image( GTK_BUTTON(this->m_native), image );
}

} // namespace gui

#endif // GUI_GTK_PUSHABLE_IPP
