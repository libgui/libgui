/*
    Name:        gui/gtk/canvas.ipp
    Purpose:     GTK drawable widget implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/21
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_CANVAS_IPP
#define GUI_GTK_CANVAS_IPP

#include <iostream>

namespace gui {

template <typename T>
canvas<T>::canvas( widget<T> *parent )
{
    this->m_native = gtk_drawing_area_new();

    widget<T>::post_creation();
}

template <typename T>
canvas<T>::~canvas()
{
}

} // namespace gui

#endif // GUI_GTK_CANVAS_IPP
