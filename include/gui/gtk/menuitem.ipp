/*
    Name:        gui/gtk/menuitem.ipp
    Purpose:     GTK menuitem implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/07
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_MENUITEM_IPP
#define GUI_GTK_MENUITEM_IPP

#include <iostream>

static void
gtk_callback_activate_event( GtkWidget */*gtkitem*/,
                             gui::menuitem<gui::gtk> *item )
{
    item->sig_select(item);
}

namespace gui {

template <typename T>
menuitem<T>::menuitem( const std::string &text )
:
m_label(text),
m_native(nullptr)
{
    m_native = gtk_menu_item_new_with_mnemonic( text.c_str() );

    g_signal_connect( m_native, "activate",
                      G_CALLBACK(gtk_callback_activate_event), this );
}

template <typename T>
menuitem<T>::~menuitem()
{
}

template <typename T>
int menuitem<T>::id() const
{
//  Not implemented
    return -1;
}

} // namespace gui

#endif // GUI_GTK_MENUITEM_IPP
