/*
    Name:        gui/gtk/separator.ipp
    Purpose:     GTK separator implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/29
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_SEPARATOR_IPP
#define GUI_GTK_SEPARATOR_IPP

namespace gui {

template <typename T>
separator<T>::separator( widget<T> *parent, orientation_t orientation )
:
widget<T>::widget(parent)
{
    GtkOrientation gtkorientation = orientation & vertical ?
                                    GTK_ORIENTATION_VERTICAL :
                                    GTK_ORIENTATION_HORIZONTAL;
    this->m_native = gtk_separator_new( gtkorientation );
}

template <typename T>
separator<T>::~separator()
{
}

} // namespace gui

#endif // GUI_GTK_SEPARATOR_IPP
