/*
    Name:        gui/graphic/context.hpp
    Purpose:     Win32 drawing context class implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/09
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_GFX_CONTEXT_IPP
#define GUI_GTK_GFX_CONTEXT_IPP

namespace gui {
namespace gfx {

template <typename T>
context<T>::context()
:
m_context(NULL)
{
}

template <typename T>
context<T>::context(native_gc_t ctx)
:
m_context(ctx)
{
}

template <typename T>
context<T>::~context()
{
}

template <typename T>
void context<T>::line_to(double x, double y)
{
    cairo_line_to(m_context, x, y);
}

template <typename T>
void context<T>::move_to(double x, double y)
{
    cairo_move_to(m_context, x, y);
}

template <typename T>
void context<T>::stroke()
{
    cairo_stroke(m_context);
}

template <typename T>
void context<T>::set_source_rgba(double r, double g, double b, double a)
{
    cairo_set_source_rgba(m_context, r, g, b, a);
}

template <typename T>
void context<T>::set_line_width(double width)
{
    cairo_set_line_width(m_context, width);
}

} // namespace gfx;
} // namespace gui;

#endif // GUI_GTK_GFX_CONTEXT_IPP
