/*
    Name:        gui/gtk/button.ipp
    Purpose:     GTK button implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/22
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_BUTTON_IPP
#define GUI_GTK_BUTTON_IPP

namespace gui {

template<typename T>
button<T>::button( widget<T> *parent )
:
pushable<T>::pushable(parent)
{
    this->m_native = gtk_button_new_with_mnemonic("");

    g_signal_connect( this->m_native, "clicked",
                      G_CALLBACK(gtk_callback_clicked_event), this );
}

template<typename T>
button<T>::~button()
{
}

} // namespace gui

#endif // GUI_GTK_BUTTON_IPP
