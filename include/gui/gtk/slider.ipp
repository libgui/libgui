/*
    Name:        gui/gtk/slider.ipp
    Purpose:     GTK slider implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_SLIDER_IPP
#define GUI_GTK_SLIDER_IPP

namespace gui {

template<typename T>
slider<T>::slider( widget<T> *parent, orientation_t orientation )
:
range<T>::range( parent, orientation )
{
    bool isVertical = orientation == vertical ? true : false;

    this->m_native = gtk_scale_new( GtkOrientation(isVertical), NULL );
}

template<typename T>
slider<T>::~slider()
{
}

template<typename T>
void slider<T>::do_set_min( int min )
{
    gtk_range_set_range( GTK_RANGE(this->m_native), min, this->m_max );
}

template<typename T>
void slider<T>::do_set_max( int max )
{
    gtk_range_set_range( GTK_RANGE(this->m_native), this->m_min, max );
}

template<typename T>
void slider<T>::do_set_position( int position )
{
    gtk_range_set_value( GTK_RANGE(this->m_native), position );
}

} // namespace gui

#endif // GUI_GTK_SLIDER_IPP
