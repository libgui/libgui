/*
    Name:        gui/gtk/widget.ipp
    Purpose:     GTK widget implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_WIDGET_IPP
#define GUI_GTK_WIDGET_IPP

#include <iostream>

#include "gui/gfx/context.hpp"

static gboolean
gtk_callback_draw_event(GtkWidget */*widget*/,
                        cairo_t *cr,
                        gui::widget<gui::gtk> *w)
{
    if( w )
    {
        gui::gfx::context<gui::gtk> ctx(cr);
        w->sig_paint(ctx);
    }
    return false;
}

namespace gui {

template <typename T>
widget<T>::widget()
:
m_native(nullptr),
m_parent(nullptr),
m_isToplevel(false),
m_enabled(true)
{
}

template <typename T>
widget<T>::widget(native_widget_t native)
:
m_native(native),
m_parent(nullptr),
m_isToplevel(false),
m_enabled(true)
{
}

template <typename T>
widget<T>::widget(widget<T> *parent)
:
m_native(nullptr),
m_parent(parent),
m_isToplevel(false),
m_enabled(true)
{
}

template <typename T>
widget<T>::~widget()
{
}

template <typename T>
void widget<T>::post_creation()
{
    g_signal_connect( m_native, "draw",
                      G_CALLBACK (gtk_callback_draw_event), this );
}

template <typename T>
void widget<T>::add( widget<T> *child )
{
    gtk_container_add( GTK_CONTAINER(m_native), child->native() );
}

template <typename T>
void widget<T>::destroy()
{
    this->sig_destroy(this);
    gtk_widget_destroy( m_native );
    m_native = nullptr;
}

template <typename T>
void widget<T>::do_enable( bool enable )
{
    if( !m_native || (enable == m_enabled) )
        return;

    gtk_widget_set_sensitive( m_native, enable );
    m_enabled = enable;
}

template <typename T>
void widget<T>::do_set_size( int width, int height )
{
    // TODO
}

template <typename T>
void widget<T>::expand( orientation_t orientation )
{
    bool b;
    b = orientation & horizontal ? true : false; gtk_widget_set_hexpand( m_native, b );
    b = orientation & vertical   ? true : false; gtk_widget_set_vexpand( m_native, b );
}

template <typename T>
int widget<T>::height() const
{
    GtkAllocation allocation;
    gtk_widget_get_allocation( m_native, &allocation );
    return allocation.height;
}

template <typename T>
void widget<T>::hide()
{
    gtk_widget_hide( m_native );
}

template <typename T>
int widget<T>::width() const
{
    GtkAllocation allocation;
    gtk_widget_get_allocation( m_native, &allocation );
    return allocation.width;
}

template <typename T>
void widget<T>::show()
{
    gtk_widget_show( m_native );
}

template <typename T>
void widget<T>::show_all()
{
    gtk_widget_show_all( m_native );
}

} // namespace gui

#endif // GUI_GTK_WIDGET_IPP
