/*
    Name:        gui/gtk/toplevel.ipp
    Purpose:     GTK toplevel implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_TOPLEVEL_IPP
#define GUI_GTK_TOPLEVEL_IPP

static gboolean
gtk_callback_delete_event( GtkWidget */* widget */,
                           GdkEvent  */* event  */,
                           gui::toplevel<gui::gtk> *tlw )
{
    if( tlw->is_enabled() )
        tlw->close();

    return TRUE;
}

namespace gui {

bool _default_on_close( toplevel<gtk>* ) { return true; }

template <typename T>
toplevel<T>::toplevel( toplevel<T> *parent )
:
widget<T>::widget( parent )
{
    this->m_isToplevel = true;
    sig_close.connect( &_default_on_close );
}

template <typename T>
toplevel<T>::~toplevel()
{
}

template <typename T>
void toplevel<T>::close()
{
    if( *sig_close(this) )
    {
        this->destroy();
    }
}

template <typename T>
void toplevel<T>::set_title( const std::string &title )
{
    gtk_window_set_title( GTK_WINDOW(this->m_native), title.c_str() );
}

template <typename T>
std::string toplevel<T>::title() const
{
    return gtk_window_get_title( GTK_WINDOW(this->m_native) );
}

} // namespace gui

#endif // GUI_GTK_TOPLEVEL_IPP
