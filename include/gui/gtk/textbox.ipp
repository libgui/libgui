/*
    Name:        gui/gtk/textbox.ipp
    Purpose:     GTK textbox implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_TEXTBOX_IPP
#define GUI_GTK_TEXTBOX_IPP

#include <iostream>

namespace gui {

template <typename T>
textbox<T>::textbox( widget<T> *parent )
:
widget<T>::widget( parent )
{
    this->m_native = gtk_entry_new();
}

template <typename T>
textbox<T>::~textbox()
{
}

template <typename T>
void textbox<T>::set_text(const std::string &text)
{
    gtk_entry_set_text( GTK_ENTRY( this->m_native ), text.c_str() );
}

} // namespace gui

#endif // GUI_GTK_TEXTBOX_IPP
