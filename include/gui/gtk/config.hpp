/*
    Name:        gui/gtk/config.hpp
    Purpose:     GTK config
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/14
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_CONFIG_HPP
#define GUI_GTK_CONFIG_HPP

#include <gtk/gtk.h>

namespace gui {

    struct  gtk {};

    typedef gtk         native_backend_t;
    typedef GtkWidget * native_widget_t;
    typedef GtkWidget * native_menu_t;
    typedef GtkWidget * native_menuitem_t;
    typedef cairo_t   * native_gc_t;

} // namespace gui

#endif // GUI_GTK_CONFIG_HPP
