/*
    Name:        gui/gtk/radiobutton.ipp
    Purpose:     GTK radiobutton implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_RADIOBUTTON_IPP
#define GUI_GTK_RADIOBUTTON_IPP

namespace gui {

template<typename T>
radiobutton<T>::radiobutton( widget<T> *parent )
:
pushable<T>::pushable(parent)
{
    this->m_native =  gtk_radio_button_new_with_mnemonic(NULL, "");

    g_signal_connect( this->m_native, "clicked",
                      G_CALLBACK(gtk_callback_clicked_event), this );
}

template<typename T>
radiobutton<T>::~radiobutton()
{
}

} // namespace gui

#endif // GUI_GTK_RADIOBUTTON_IPP
