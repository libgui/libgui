/*
    Name:        gui/gtk/eventloop.ipp
    Purpose:     GTK eventloop implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_EVENTLOOP_IPP
#define GUI_GTK_EVENTLOOP_IPP

#include <iostream>

namespace gui {

template <typename T>
eventloop<T>::eventloop()
{
}

template <typename T>
eventloop<T>::~eventloop()
{
}

template <typename T>
void eventloop<T>::exit()
{
    gtk_main_quit();
}

template <typename T>
void eventloop<T>::run()
{
    gtk_main();
}

} // namespace gui

#endif // GUI_GTK_EVENTLOOP_IPP
