/*
    Name:        gui/gtk/notebook.ipp
    Purpose:     GTK notebook implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/03
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_NOTEBOOK_IPP
#define GUI_GTK_NOTEBOOK_IPP

#include <iostream>

namespace gui {

template <typename T>
notebook<T>::notebook(widget<T> *parent)
:
bookshell<T>::bookshell(parent)
{
    this->m_native = gtk_notebook_new();
}

template <typename T>
notebook<T>::~notebook()
{
}

template <typename T>
int notebook<T>::do_insert_page(widget<T> *page,
                                const std::string& label,
                                size_t position)
{
    int index = not_found;
    if( !page )
        return index;

    GtkNotebook *notebook = GTK_NOTEBOOK( this->m_native );

    index = gtk_notebook_insert_page( notebook, page->native(), NULL, position );

    gtk_notebook_set_tab_label_text( notebook, page->native(), label.c_str() );

    return index;
}

template <typename T>
void notebook<T>::do_set_selection( size_t index )
{
    gtk_notebook_set_current_page( GTK_NOTEBOOK( this->m_native ), index );
}

template <typename T>
void notebook<T>::set_show_tabs(bool show)
{
    gtk_notebook_set_show_tabs( GTK_NOTEBOOK(this->m_native), show );
}

template <typename T>
void notebook<T>::set_scrollable(bool scrollable)
{
    gtk_notebook_set_scrollable( GTK_NOTEBOOK(this->m_native), scrollable );
}

} // namespace gui

#endif // GUI_GTK_NOTEBOOK_IPP
