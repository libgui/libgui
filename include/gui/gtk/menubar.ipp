/*
    Name:        gui/gtk/menubar.ipp
    Purpose:     GTK menubar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_MENUBAR_IPP
#define GUI_GTK_MENUBAR_IPP

namespace gui {

template <typename T>
menubar<T>::menubar()
{
}

template <typename T>
menubar<T>::menubar( widget<T> *parent )
:
widget<T>::widget(parent)
{
    this->m_native = gtk_menu_bar_new();
}

template <typename T>
menubar<T>::~menubar()
{
}

template <typename T>
void menubar<T>::add_menu( menu<T> *menu, const std::string &title )
{
    GtkWidget *item = gtk_menu_item_new_with_mnemonic( title.c_str() );

    gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), menu->native() );

    gtk_menu_shell_append( GTK_MENU_SHELL(this->m_native), item );
}

} // namespace gui

#endif // GUI_GTK_MENUBAR_IPP
