/*
    Name:        gui/gtk/window.ipp
    Purpose:     GTK window implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GTK_WINDOW_IPP
#define GUI_GTK_WINDOW_IPP

namespace gui {

template <typename T>
window<T>::window( toplevel<T> *parent )
:
toplevel<T>::toplevel( parent )
{
    this->m_native = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect( this->m_native, "delete_event",
                      G_CALLBACK(gtk_callback_delete_event), this );

    widget<T>::post_creation();
}

template <typename T>
window<T>::~window()
{
}

template <typename T>
void window<T>::do_set_size( int width, int height )
{
    gtk_window_resize( GTK_WINDOW(this->m_native), width, height );
}

template <typename T>
void window<T>::show_resize_grip( bool show )
{
#if GTK_MINOR_VERSION < 14
    gtk_window_set_has_resize_grip( GTK_WINDOW(this->m_native), show );
#endif
}

} // namespace gui

#endif // GUI_GTK_WINDOW_IPP
