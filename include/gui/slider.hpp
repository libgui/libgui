/*
    Name:        gui/slider.hpp
    Purpose:     Slider widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/26
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_SLIDER_HPP
#define GUI_SLIDER_HPP

#include "gui/geometry.hpp"
#include "gui/slider.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class slider

    Slider.
*/
class slider : public range<T>
{
public:
//  No child widgets without parent.
    slider() = delete;
/**
    Constructor.
    @param parent      The parent widget.
    @param orientation Horizontal or vertical orientation.
*/
    slider( widget<T> *parent, orientation_t orientation = horizontal );

/** Destructor. */
    ~slider();

private:
    void do_set_min( int min );
    void do_set_max( int max );
    void do_set_position( int position );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/slider.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/slider.ipp"
#endif

#endif // GUI_SLIDER_HPP
