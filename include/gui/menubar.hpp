/*
    Name:        gui/menubar.hpp
    Purpose:     Menubar interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_MENUBAR_HPP
#define GUI_MENUBAR_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"
#include "gui/menu.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class menubar

    Menu bar.

    @todo Add function:
        `native_menu_t native() const`
*/
class menubar : public widget<T>
{
public:
/** Constructor. */
    menubar();
/**
    Constructor.
    @param parent The parent widget.
*/
    menubar( widget<T> *parent );

/** Destructor. */
    ~menubar();
/**
    Adds a menu to the menubar.
    @param menu The menu to add.
    @param label The label to assign to the added menu.
*/
    void add_menu( menu<T> *menu, const std::string &label );

private:
//  Unused in some ports.
    native_menu_t m_menubar;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/menubar.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/menubar.ipp"
#endif

#endif // GUI_MENUBAR_HPP
