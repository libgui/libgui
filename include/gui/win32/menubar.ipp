/*
    Name:        gui/win32/menubar.ipp
    Purpose:     Win32 menubar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_MENUBAR_IPP
#define GUI_WIN32_MENUBAR_IPP

namespace gui {

template <typename T>
menubar<T>::menubar()
{
    m_menubar = ::CreateMenu();
}

template <typename T>
menubar<T>::menubar( widget<T> *parent )
{
    m_menubar = ::CreateMenu();

    // Call WM_MENUCOMMAND
    MENUINFO mi;
    mi.cbSize  = sizeof( MENUINFO );
    mi.fMask   = MIM_STYLE;
    mi.dwStyle = MNS_NOTIFYBYPOS;

    if( !::SetMenuInfo( m_menubar, &mi ) )
    {
        DWORD lastError = GetLastError();
        std::cerr << "SetMenuInfo() error in menubar: "<< lastError << '\n';
    }

    ::SetMenu( parent->native(), m_menubar );
}

template <typename T>
menubar<T>::~menubar()
{
}

template <typename T>
void menubar<T>::add_menu( menu<T> *menu, const std::string &title )
{
    ::AppendMenuW( m_menubar,
                    MF_STRING | MF_POPUP,
                    (UINT)menu->native(),
                    nowide::widen(title).c_str() );
}

} // namespace gui

#endif // GUI_WIN32_MENUBAR_IPP
