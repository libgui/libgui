/*
    Name:        gui/win32/button.ipp
    Purpose:     GTK button implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/23
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_BUTTON_IPP
#define GUI_WIN32_BUTTON_IPP

#include "gui/win32/detail/shell.hpp"

namespace gui {

template<typename T>
button<T>::button( widget<T> *parent )
{
    this->m_native = ::CreateWindowExW( 0,
                                        L"Button",
                                        NULL,
                                        WS_CHILD | WS_VISIBLE,
                                        100, 50,
                                        140, 50,
                                        parent->native(),
                                        NULL,
                                        GetModuleHandle(NULL), NULL );
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the button: "<< lastError << '\n';
    }
}

template<typename T>
button<T>::~button()
{
}

template<typename T>
void button<T>::set_label( const std::string &label )
{
    ::SetWindowTextW( this->m_native, nowide::widen(label).c_str() ); 
}

template<typename T>
void button<T>::set_image( const std::string &name )
{
//  Not implemented.
}

} // namespace gui

#endif // GUI_WIN32_BUTTON_IPP
