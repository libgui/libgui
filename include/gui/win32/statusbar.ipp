/*
    Name:        gui/win32/statusbar.ipp
    Purpose:     Win32 statusbar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_STATUSBAR_IPP
#define GUI_WIN32_STATUSBAR_IPP

#include <iostream>

namespace gui {

template <typename T>
statusbar<T>::statusbar( widget<T> *parent )
{
    this->m_native = ::CreateWindowExW( 0,
                                        L"msctls_statusbar32",
                                        NULL,
                                        WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
                                        CW_USEDEFAULT, CW_USEDEFAULT,
                                        CW_USEDEFAULT, CW_USEDEFAULT,
                                        parent->native(),
                                        (HMENU)999,
                                        ::GetModuleHandleW(NULL), NULL );
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the statusbar: "<< lastError << std::endl;
    }
/*
int statwidths[] = {100, -1};
::SendMessageW(this->m_native, SB_SETPARTS, sizeof(statwidths)/sizeof(int), (LPARAM)statwidths);
*/
}

template <typename T>
statusbar<T>::~statusbar()
{
}

template <typename T>
void statusbar<T>::set_text(const std::string &text)
{
    ::SendMessageW( this->m_native, SB_SETTEXT, 0,
                    (LPARAM)nowide::widen(text).c_str() );
}

} // namespace gui

#endif // GUI_WIN32_STATUSBAR_IPP
