/*
    Name:        gui/win32/notebook.ipp
    Purpose:     Notebook widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/06
    Revision:    $ID$
    Licence:     ???
*/
#ifndef GUI_WIN32_NOTEBOOK_HPP
#define GUI_WIN32_NOTEBOOK_HPP

#include <nowide/convert.hpp>

namespace gui {

template <typename T>
notebook<T>::notebook(widget<T> *parent)
:
bookshell<T>::bookshell(parent)
{
    // TODO: remove this once layout management is done.
    RECT parentRect;
    GetClientRect(parent->native(), &parentRect); 

    this->m_native = ::CreateWindowExW( 0,
                                        WC_TABCONTROLW,
                                        NULL,
                                        WS_CHILD | WS_VISIBLE |
                                        TCS_TABS,
                                        // TCS_BUTTONS | TCS_FLATBUTTONS
                                        parentRect.top, parentRect.left,
                                        parentRect.right, parentRect.bottom,
/*
                                        CW_USEDEFAULT, CW_USEDEFAULT,
                                        CW_USEDEFAULT, CW_USEDEFAULT,
*/
                                        parent->native(),
                                        NULL,
                                        ::GetModuleHandleW(NULL), NULL);
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the notebook: "<< lastError << std::endl;
    }
}

template <typename T>
notebook<T>::~notebook()
{
}

template <typename T>
int notebook<T>::do_insert_page( widget<T> *page,
                                 const std::string &label,
                                 size_t index )
{
    if( !page || index > this->m_pages.size() )
        return -1;

    TCITEMW tabCtrlItem;
    tabCtrlItem.mask    = TCIF_TEXT;// | TCIF_IMAGE; 
//  tabCtrlItem.iImage  = -1;

    // For some weird reason this works using wstring in mingw-builds gdb,
    // using wchar_t instead result is a crash
    std::wstring wide   = nowide::widen(label);
    tabCtrlItem.pszText = const_cast<wchar_t *>( wide.c_str() );

    int newIndex = ::SendMessageW( this->m_native, TCM_INSERTITEMW,
                                   (WPARAM)index, (LPARAM)&tabCtrlItem );
    return newIndex;
}

template <typename T>
void notebook<T>::do_set_selection( size_t index )
{
    if( !this->m_pages.empty() && index < this->m_pages.size() )
        ::SendMessageW( this->m_native, TCM_SETCURSEL, (WPARAM)index, 0 );
}

template <typename T>
void notebook<T>::set_show_tabs( bool show )
{
//  TODO
}

template <typename T>
void notebook<T>::set_scrollable( bool scrollable )
{
//  TODO
}

} // namespace gui

#endif // GUI_WIN32_NOTEBOOK_HPP
