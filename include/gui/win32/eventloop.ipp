/*
    Name:        gui/win32/eventloop.ipp
    Purpose:     Win32 eventloop implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/12
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_EVENTLOOP_IPP
#define GUI_WIN32_EVENTLOOP_IPP

namespace gui {

template <typename T>
eventloop<T>::eventloop()
{
}

template <typename T>
eventloop<T>::~eventloop()
{
}

template <typename T>
void eventloop<T>::exit()
{
//  TODO
}

template <typename T>
void eventloop<T>::run()
{
    MSG msg;

    while( ::GetMessage(&msg, NULL, 0, 0) )
    {
        ::TranslateMessage(&msg);
        ::DispatchMessage(&msg);
    }

//  return (int)msg.wParam;
}

} // namespace gui

#endif // GUI_WIN32_EVENTLOOP_IPP
