/*
    Name:        gui/win32/textbox.ipp
    Purpose:     Win32 textbox implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_TEXTBOX_IPP
#define GUI_WIN32_TEXTBOX_IPP

#include <nowide/convert.hpp>

namespace gui {

template <typename T>
textbox<T>::textbox( widget<T> *parent )
{
    this->m_native = ::CreateWindowExW( WS_EX_CLIENTEDGE,
                                        WC_EDITW,
                                        NULL,
                                        WS_CHILD | WS_VISIBLE ,
                                        100, 20,
                                        140, 20,
                                        parent->native(),
                                        NULL,
                                        GetModuleHandle(NULL), NULL );

    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the textbox: "<< lastError << std::endl;
    }
}

template <typename T>
textbox<T>::~textbox()
{
}

template <typename T>
void textbox<T>::set_text(const std::string &text)
{
    ::SetWindowTextW( this->m_native, nowide::widen(text).c_str() ); 
}

} // namespace gui

#endif // GUI_WIN32_TEXTBOX_IPP
