/*
    Name:        gui/win32/progressbar.ipp
    Purpose:     Win32 progress bar implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/23
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_PROGRESSBAR_IPP
#define GUI_WIN32_PROGRESSBAR_IPP

namespace gui {

template<typename T>
progressbar<T>::progressbar( widget<T> *parent, orientation_t orientation )
:
range<T>::range( parent, orientation )
{
    DWORD dwStyle = WS_CHILD | WS_VISIBLE;

    if( orientation & vertical )
        dwStyle |= PBS_VERTICAL;

    this->m_native = ::CreateWindowExW( 0,
                                        PROGRESS_CLASSW,
                                        NULL,
                                        dwStyle,
                                        100, 120,
                                        140, 20,
                                        parent->native(),
                                        NULL,
                                        GetModuleHandle(NULL), NULL );
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the progress bar: "<< lastError << std::endl;
    }
}

template<typename T>
progressbar<T>::~progressbar()
{
}

template<typename T>
void progressbar<T>::do_set_min( int min )
{
    ::SendMessageW( this->m_native, PBM_SETRANGE32, min, this->m_max );
}

template<typename T>
void progressbar<T>::do_set_max( int max )
{
    ::SendMessageW( this->m_native, PBM_SETRANGE32, this->m_min, max );
}

template<typename T>
void progressbar<T>::do_set_position( int position )
{
    if( ::GetWindowLongW(this->m_native, GWL_STYLE) & PBS_MARQUEE )
    {
        const long style = ::GetWindowLongW( this->m_native, GWL_STYLE );
        ::SetWindowLongW( this->m_native, GWL_STYLE, style &~ PBS_MARQUEE);
        ::SendMessageW  ( this->m_native, PBM_SETMARQUEE, FALSE, 0);
    }

    ::SendMessageW( this->m_native, PBM_SETPOS, position, 0 );
}

template<typename T>
void progressbar<T>::pulse()
{
    if( ::GetWindowLongW(this->m_native, GWL_STYLE) &~ PBS_MARQUEE )
    {
        const long style = ::GetWindowLongW( this->m_native, GWL_STYLE );
        ::SetWindowLongW( this->m_native, GWL_STYLE, style | PBS_MARQUEE );
        ::SendMessageW  ( this->m_native, PBM_SETMARQUEE, TRUE, 0 );
    }
}

} // namespace gui

#endif // GUI_WIN32_PROGRESSBAR_IPP
