/*
    Name:        gui/win32/gfx/context.hpp
    Purpose:     Win32 drawing context class implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/09
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_GFX_CONTEXT_IPP
#define GUI_WIN32_GFX_CONTEXT_IPP

#include <wingdi.h>
//#include <gdiplus.h> it seems GDI+ is easier to use and most complete,
//                     but slower than GDI

namespace gui {
namespace gfx {

template <typename T>
context<T>::context()
:
m_context(NULL)
{
}

template <typename T>
context<T>::context(native_gc_t ctx)
:
m_context(ctx)
{
}

template <typename T>
context<T>::~context()
{
}

template <typename T>
void context<T>::line_to(double x, double y)
{
    ::LineTo(m_context, x, y);
}

template <typename T>
void context<T>::move_to(double x, double y)
{
    ::MoveToEx(m_context, x, y, NULL);
}


template <typename T>
void context<T>::stroke()
{
    // TODO: Maybe unused on win32
}

template <typename T>
void context<T>::set_source_rgba(double r, double g, double b, double a)
{
    // TODO: Use AlphaBlend
}

template <typename T>
void context<T>::set_line_width(double width)
{
    // TODO: Draw a rectangle for width > 1?
}

} // namespace gfx;
} // namespace gui;

#endif // GUI_WIN32_GFX_CONTEXT_IPP
