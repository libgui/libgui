/*
    Name:        gui/win32/window.ipp
    Purpose:     Win32 window implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/12
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_WINDOW_IPP
#define GUI_WIN32_WINDOW_IPP

#include <iostream>
#include "gui/win32/detail/register.hpp"

namespace gui {

template <typename T>
window<T>::window( toplevel<T> *parent )
:
toplevel<T>::toplevel( parent )
{
    ::register_class( L"window", sizeof( window<win32> * ) );

    this->m_native = ::CreateWindowExW( 0,
                                        L"window",
                                        NULL,
                                        WS_OVERLAPPEDWINDOW,
                                        CW_USEDEFAULT, CW_USEDEFAULT,
                                        CW_USEDEFAULT, CW_USEDEFAULT,
                                        NULL,
                                        NULL,
                                        ::GetModuleHandleW(NULL), NULL );
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the window: "<< lastError << std::endl;
    }
}

template <typename T>
window<T>::~window()
{
}

template <typename T>
void window<T>::do_set_size( int width, int height )
{
    widget<T>::do_set_size( width, height );
}

template <typename T>
void window<T>::show_resize_grip( bool show )
{
    // TODO
}

} // namespace gui

#endif // GUI_WIN32_WINDOW_IPP
