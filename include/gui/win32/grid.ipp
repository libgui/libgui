/*
    Name:        gui/win32/grid.ipp
    Purpose:     Win32 grid implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_GRID_IPP
#define GUI_WIN32_GRID_IPP

#include <iostream>

namespace gui {

template <typename T>
grid<T>::grid()
{
}

template <typename T>
grid<T>::grid(widget<T> *parent)
{
}

template <typename T>
grid<T>::~grid()
{
}

template <typename T>
void grid<T>::attach(const widget<T> &child, int col, int row,
                            int colspan, int rowspan)
{
}

} // namespace gui

#endif // GUI_WIN32_GRID_IPP
