/*
    Name:        gui/win32/slider.ipp
    Purpose:     Win32 slider implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/26
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_SLIDER_IPP
#define GUI_WIN32_SLIDER_IPP

namespace gui {

template<typename T>
slider<T>::slider( widget<T> *parent, orientation_t orientation )
:
range<T>::range( parent, orientation )
{
    DWORD dwStyle = WS_CHILD | WS_VISIBLE;

    if( orientation & vertical )
        dwStyle |= TBS_VERT;

    this->m_native = ::CreateWindowExW( 0,
                                        TRACKBAR_CLASSW,
                                        NULL,
                                        dwStyle,
                                        100, 150,
                                        140, 20,
                                        parent->native(),
                                        NULL,
                                        GetModuleHandle(NULL), NULL );
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the slider: "<< lastError << std::endl;
    }
}

template<typename T>
slider<T>::~slider()
{
}

template<typename T>
void slider<T>::do_set_min( int min )
{
    ::SendMessageW( this->m_native, TBM_SETRANGEMIN, TRUE, min );
}

template<typename T>
void slider<T>::do_set_max( int max )
{
    ::SendMessageW( this->m_native, TBM_SETRANGEMAX, TRUE, max );
}

template<typename T>
void slider<T>::do_set_position( int position )
{
    ::SendMessageW( this->m_native, TBM_SETPOS, TRUE, position );
}

} // namespace gui

#endif // GUI_WIN32_SLIDER_IPP
