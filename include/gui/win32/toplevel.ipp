/*
    Name:        gui/win32/toplevel.ipp
    Purpose:     Win32 toplevel implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/12
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_TOPLEVEL_IPP
#define GUI_WIN32_TOPLEVEL_IPP

#include <iostream>
#include <nowide/convert.hpp>

namespace gui {

template <typename T>
toplevel<T>::toplevel( toplevel<T> *parent )
:
widget<T>::widget( parent )
{
    this->m_isToplevel = true;
}

template <typename T>
toplevel<T>::~toplevel()
{
}

template <typename T>
void toplevel<T>::close()
{
    if( *this->sig_close(this) )
    {
        this->destroy();
        // TODO: should be called in eventloop::exit()
        ::PostQuitMessage(0);
    }
}

template <typename T>
void toplevel<T>::set_title(const std::string &title)
{
    ::SetWindowTextW( this->m_native, nowide::widen(title).c_str() );
}

template <typename T>
std::string toplevel<T>::title() const
{
    std::string title;

    if( this->m_native )
    {
        int     length = ::GetWindowTextLengthW( this->m_native ) + 1;
        wchar_t wide[length];
        ::GetWindowTextW( this->m_native, wide, length);
        title = nowide::narrow( wide );
    }
    return title;
}

} // namespace gui

#endif // GUI_WIN32_TOPLEVEL_IPP
