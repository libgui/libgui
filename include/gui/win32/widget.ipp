/*
    Name:        gui/win32/widget.ipp
    Purpose:     Win32 widget implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/12
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_WIDGET_IPP
#define GUI_WIN32_WIDGET_IPP

#include <iostream>
#include <vector>

#include "gui/gfx/context.hpp"

namespace gui {

typedef std::vector< widget<win32> * > widget_vector;
widget_vector widgets;

widget<win32> *widget_from_native( native_widget_t hWnd )
{
    for( widget_vector::iterator it = widgets.begin();
                                it != widgets.end(); ++it )
        if( (*it)->native() == hWnd )
            return (*it);

    return nullptr;
}

template <typename T>
widget<T>::widget()
:
m_native(nullptr),
m_parent(nullptr),
m_isToplevel(false),
m_enabled(false)
{
    widgets.push_back( this );
}

template <typename T>
widget<T>::widget( native_widget_t native )
:
m_native(native),
m_parent(nullptr),
m_isToplevel(false),
m_enabled(false)
{
    //TODO
}

template <typename T>
widget<T>::widget( widget<T> *parent )
:
m_native(nullptr),
m_parent(parent),
m_isToplevel(false),
m_enabled(false)
{
    widgets.push_back( this );
}

template <typename T>
void widget<T>::post_creation()
{
    // TODO
}

template <typename T>
widget<T>::~widget()
{
    if( m_native )
    {
        if( !::DestroyWindow( m_native ) )
            std::cout << "DestroyWindow() Error\n";

        m_native = nullptr;
    }
}

template <typename T>
void widget<T>::add( widget<T> *child )
{
//  TODO
}

template <typename T>
void widget<T>::destroy()
{
    this->sig_destroy(this);
}

template <typename T>
void widget<T>::do_enable( bool enable )
{
    if( !m_native || (enable == m_enabled) )
        return;

    ::EnableWindow( m_native, enable );
    m_enabled = enable;
}

template <typename T>
void widget<T>::do_set_size( int width, int height )
{
    ::SetWindowPos( m_native, NULL, 0, 0, width, height, SWP_NOMOVE );
}

template <typename T>
void widget<T>::expand( orientation_t orientation )
{
    // TODO
}

template <typename T>
int widget<T>::height() const
{
    // TODO
}

template <typename T>
int widget<T>::width() const
{
    // TODO
}

template <typename T>
void widget<T>::hide()
{
    ::ShowWindow( m_native, SW_HIDE );
}

template <typename T>
void widget<T>::show()
{
    ::ShowWindow( m_native, SW_SHOWNORMAL );
}

template <typename T>
void widget<T>::show_all()
{
    ::ShowWindow( m_native, SW_SHOWNORMAL );
}

} // namespace gui

#endif // GUI_WIN32_WIDGET_IPP
