/*
    Name:        gui/win32/menushell.ipp
    Purpose:     Win32 menushell implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_MENUSHELL_IPP
#define GUI_WIN32_MENUSHELL_IPP

#include <iostream>

template <typename T>
menushell<T>::menushell()
{
}

template <typename T>
menushell<T>::~menushell()
{
}

template <typename T>
void menushell<T>::push_back( menuitem<T> *item )
{
    ::AppendMenuW( m_menu, MF_STRING, (UINT)item->native(), item->text().c_str() );
/*
    HMENU hSubMenu = ::CreatePopupMenu();

    AppendMenu( m_menu, MF_STRING | MF_POPUP, (UINT)hSubMenu, "&Stuff" );
    AppendMenu( hSubMenu, MF_STRING, 0, "&Other Stuff" );
*/
}

template <typename T>
void menushell<T>::push_front( menuitem<T> *item )
{
}

template <typename T>
void menushell<T>::insert( int position, menuitem<T> *item )
{
}

} // namespace gui

#endif // GUI_WIN32_MENUSHELL_IPP
