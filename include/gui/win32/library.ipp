/*
    Name:        gui/win32/library.ipp
    Purpose:     Win32 library version
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/06
    Revision:    $ID$
    Licence:     ???
*/
#ifndef GUI_WIN32_LIBRARY_IPP
#define GUI_WIN32_LIBRARY_IPP

#include <iostream>

#include <windef.h>
#include <winbase.h>
#include <shlwapi.h>

#define LODWORD(l) ((DWORD)((DWORDLONG)(l)))
#define PACKVERSION(major,minor) MAKELONG(minor,major)
DWORD GetVersion(LPCTSTR dllName)
{
/*
    DWORD dwVersion = 0;
    HMODULE hDll;
    std::string path("W:\\Git\\libgui\\buildmsw\\bin\\COMCTL32.DLL");
    printf(path.c_str());
    hDll = LoadLibrary(path.c_str());
    if(!hDll)
    {
        printf("GetDllVersion: LoadLibrary failed.");
        return dwVersion;
    };

    FARPROC pDllGetVersion;
    pDllGetVersion = GetProcAddress(hDll, "DllGetVersion\n");
    if(!pDllGetVersion)
    {
        printf("GetDllVersion: DllGetVersion failed.");
        return dwVersion;
    };

    DLLVERSIONINFO2 dvi;
    ZeroMemory(&dvi, sizeof(dvi));
    dvi.info1.cbSize = sizeof (dvi);
    pDllGetVersion();

    dwVersion = dvi.info1.dwMajorVersion *100 + dvi.info1.dwMinorVersion;

    printf ("MajorVersion:         %lu\n", dvi.info1.dwMajorVersion);
    printf ("MinorVersion:         %lu\n", dvi.info1.dwMinorVersion);
    printf ("BuildNumber:          %lu\n", dvi.info1.dwBuildNumber);
    printf ("RevisionNumber (QFE): %u\n", LOWORD(LODWORD(dvi.ullVersion)));
    printf ("ullVersion:           %#I64X\n\n", dvi.ullVersion);

    printf ("4.00: %i\n", MAKEWORD(4,00)); 
    printf ("4.70: %i\n", MAKEWORD(4,70)); 
    printf ("4.71: %i\n", MAKEWORD(4,71)); 
    printf ("4.72: %i\n", MAKEWORD(4,72)); 
    printf ("5.80: %i\n", MAKEWORD(5,80)); 
    printf ("5.81: %i\n", MAKEWORD(5,81)); 
    printf ("5.82: %i\n", MAKEWORD(5,82)); 
    printf ("6.00: %i\n", MAKEWORD(6,00)); 
    printf ("6.10: %i\n", LOWORD(610));

    return dwVersion;
*/
LPCTSTR szVersionFile = L"W:\\Git\\libgui\\buildmsw\\bin\\COMCTL32.DLL";
DWORD  verHandle = 0;
UINT   size      = 0;
LPBYTE lpBuffer  = NULL;
DWORD  verSize   = GetFileVersionInfoSize( szVersionFile, &verHandle);

if (verSize)
{
    LPSTR verData = new char[verSize];

    if (GetFileVersionInfo( szVersionFile, verHandle, verSize, verData))
    {
        if (VerQueryValue(verData, L"\\",(VOID FAR* FAR*)&lpBuffer,&size))
        {
            if (size)
            {
                VS_FIXEDFILEINFO *verInfo = (VS_FIXEDFILEINFO *)lpBuffer;
                if (verInfo->dwSignature == 0xfeef04bd)
                {
                    int major = HIWORD(verInfo->dwFileVersionMS);
                    int minor = LOWORD(verInfo->dwFileVersionMS);
                    int build = verInfo->dwFileVersionLS;
                }
            }
        }
    }

    delete[] verData;
}
}

#endif // GUI_WIN32_LIBRARY_IPP
