/*
    Name:        gui/win32/application.ipp
    Purpose:     Win32 application implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/12
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_APPLICATION_IPP
#define GUI_WIN32_APPLICATION_IPP

namespace gui {

template <typename T>
void application<T>::init()
{
    INITCOMMONCONTROLSEX icc;

    // Initialise common controls.
    icc.dwSize = sizeof(icc);
    icc.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&icc);
}

template <typename T>
application<T>::~application()
{
    if( m_loop )
        m_loop->exit();
}

template <typename T>
void application<T>::run()
{
    if( m_loop )
        m_loop->run();
}

} // namespace gui

#endif // GUI_WIN32_APPLICATION_IPP
