/*
    Name:        gui/win32/detail/shell.hpp
    Purpose:     Extract system icons
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/29
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_DETAIL_SHELL_HPP
#define GUI_WIN32_DETAIL_SHELL_HPP

#include <shellapi.h>

namespace gui { namespace detail { namespace win32 {

static HICON GetShellIcon( std::wstring index, int width, int height,
                           std::wstring file = L"shell32" )
{
    HMODULE hShell =
    ::LoadLibraryExW( file.c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE );

    HANDLE hIcon  =
    ::LoadImageW( hShell, index.c_str(), IMAGE_ICON, width, height, LR_SHARED );

    return reinterpret_cast<HICON>(hIcon);
}

static HICON GetShellIconByName( std::string id, int width = 16, int height = 16 )
{
    std::wstring iconIndex;
    std::wstring file = L"shell32";
// actions
    if     ( id == "address-book-new" )                       iconIndex = L"#0"; // TODO
    else if( id == "application-exit" )                       iconIndex = L"#0"; // TODO
    else if( id == "appointment-new" )                        iconIndex = L"#0"; // TODO
    else if( id == "bookmark-new" )                           iconIndex = L"#0"; // TODO
    else if( id == "contact-new" )                            iconIndex = L"#0"; // TODO
    else if( id == "dialog-cancel" )                          iconIndex = L"#0"; // TODO
    else if( id == "document-new" )                           iconIndex = L"#0"; // TODO
    else if( id == "document-open" )                          iconIndex = L"#46";
    else if( id == "document-page-setup" )                    iconIndex = L"#0"; // TODO
    else if( id == "document-print" )                         iconIndex = L"#17";
    else if( id == "document-print-preview" )                 iconIndex = L"#0"; // TODO
    else if( id == "document-properties" )                    iconIndex = L"#0"; // TODO
    else if( id == "document-revert" )                        iconIndex = L"#0"; // TODO
    else if( id == "document-save" )                          iconIndex = L"#16761";
    else if( id == "document-save-as" )                       iconIndex = L"#0"; // TODO
    else if( id == "edit-clear" )                             iconIndex = L"#0"; // TODO
    else if( id == "edit-copy" )                              iconIndex = L"#243";
    else if( id == "edit-cut" )                               iconIndex = L"#16762"; // 7
    else if( id == "edit-delete" )                            iconIndex = L"#16777"; // 7
    else if( id == "edit-find" )                              iconIndex = L"#134";
    else if( id == "edit-find-replace" )                      iconIndex = L"#0"; // TODO
    else if( id == "edit-paste" )                             iconIndex = L"#16763"; // 7
    else if( id == "edit-redo" )                              iconIndex = L"#0"; // TODO
    else if( id == "edit-select-all" )                        iconIndex = L"#0"; // TODO
    else if( id == "edit-undo" )                              iconIndex = L"#0"; // TODO
    else if( id == "folder-new" )                             iconIndex = L"#319";
    else if( id == "format-indent-less" )                     iconIndex = L"#0"; // TODO
    else if( id == "format-indent-more" )                     iconIndex = L"#0"; // TODO
    else if( id == "format-justify-center" )                  iconIndex = L"#0"; // TODO
    else if( id == "format-justify-fill" )                    iconIndex = L"#0"; // TODO
    else if( id == "format-justify-left" )                    iconIndex = L"#0"; // TODO
    else if( id == "format-justify-right" )                   iconIndex = L"#0"; // TODO
    else if( id == "format-text-bold" )                       iconIndex = L"#0"; // TODO
    else if( id == "format-text-italic" )                     iconIndex = L"#0"; // TODO
    else if( id == "format-text-strikethrough" )              iconIndex = L"#0"; // TODO
    else if( id == "format-text-underline" )                  iconIndex = L"#0"; // TODO
    else if( id == "go-bottom" )                              iconIndex = L"#0"; // TODO
    else if( id == "go-down" )                                iconIndex = L"#0"; // TODO
    else if( id == "go-first" )                               iconIndex = L"#0"; // TODO
    else if( id == "go-home" )                                iconIndex = L"#0"; // TODO
    else if( id == "go-jump" )                                iconIndex = L"#0"; // TODO
    else if( id == "go-last" )                                iconIndex = L"#0"; // TODO
    else if( id == "go-next" )                                iconIndex = L"#0"; // TODO
    else if( id == "go-previous" )                            iconIndex = L"#0"; // TODO
    else if( id == "go-top" )                                 iconIndex = L"#0"; // TODO
    else if( id == "go-up" )                                  iconIndex = L"#0"; // TODO
    else if( id == "help-about" )                             iconIndex = L"#16783"; // 7
    else if( id == "help-contents" )                          iconIndex = L"#24";
    else if( id == "list-add" )                               iconIndex = L"#0"; // TODO
    else if( id == "list-remove" )                            iconIndex = L"#0"; // TODO
    else if( id == "mail-forward" )                           iconIndex = L"#0"; // TODO
    else if( id == "mail-mark-junk" )                         iconIndex = L"#0"; // TODO
    else if( id == "mail-mark-not-junk" )                     iconIndex = L"#0"; // TODO
    else if( id == "mail-message-new" )                       iconIndex = L"#0"; // TODO
    else if( id == "mail-reply-all" )                         iconIndex = L"#0"; // TODO
    else if( id == "mail-reply-sender" )                      iconIndex = L"#0"; // TODO
    else if( id == "mail-send" )                              iconIndex = L"#0"; // TODO
    else if( id == "mail-send-receive" )                      iconIndex = L"#0"; // TODO
    else if( id == "media-eject" )                            iconIndex = L"#0"; // TODO
    else if( id == "media-playback-pause" )                   iconIndex = L"#0"; // TODO
    else if( id == "media-playback-start" )                   iconIndex = L"#0"; // TODO
    else if( id == "media-playback-stop" )                    iconIndex = L"#0"; // TODO
    else if( id == "media-record" )                           iconIndex = L"#0"; // TODO
    else if( id == "media-seek-backward" )                    iconIndex = L"#0"; // TODO
    else if( id == "media-seek-forward" )                     iconIndex = L"#0"; // TODO
    else if( id == "media-skip-backward" )                    iconIndex = L"#0"; // TODO
    else if( id == "media-skip-forward" )                     iconIndex = L"#0"; // TODO
    else if( id == "process-stop" )                           iconIndex = L"#0"; // TODO
    else if( id == "system-lock-screen" )                     iconIndex = L"#0"; // TODO
    else if( id == "system-log-out" )                         iconIndex = L"#28";
    else if( id == "system-run" )                             iconIndex = L"#0"; // TODO
    else if( id == "system-search" )                          iconIndex = L"#0"; // TODO
    else if( id == "system-shutdown" )                        iconIndex = L"#0"; // TODO
    else if( id == "tab-new" )                                iconIndex = L"#0"; // TODO
    else if( id == "tools-check-spelling" )                   iconIndex = L"#0"; // TODO
    else if( id == "view-fullscreen" )                        iconIndex = L"#0"; // TODO
    else if( id == "view-refresh" )                           iconIndex = L"#0"; // TODO
    else if( id == "view-restore" )                           iconIndex = L"#0"; // TODO
    else if( id == "view-sort-ascending" )                    iconIndex = L"#0"; // TODO
    else if( id == "view-sort-descending" )                   iconIndex = L"#0"; // TODO
    else if( id == "window-close" )                           iconIndex = L"#0"; // TODO
    else if( id == "window-new" )                             iconIndex = L"#0"; // TODO
    else if( id == "zoom-fit-best" )                          iconIndex = L"#0"; // TODO
    else if( id == "zoom-in" )                                iconIndex = L"#0"; // TODO
    else if( id == "zoom-original" )                          iconIndex = L"#0"; // TODO
    else if( id == "zoom-out" )                               iconIndex = L"#0"; // TODO
// animations
    else if( id == "process-idle" )                           iconIndex = L"#0"; // TODO
    else if( id == "process-working" )                        iconIndex = L"#0"; // TODO
// apps
    else if( id == "accessories-calculator" )               { iconIndex = L"SC"; file = L"calc.exe"; }
    else if( id == "accessories-character-map" )              iconIndex = L"#0"; // TODO
    else if( id == "accessories-text-editor" )                iconIndex = L"#0"; // TODO
    else if( id == "help-browser" )                           iconIndex = L"#24";
    else if( id == "internet-mail" )                          iconIndex = L"#0"; // TODO
    else if( id == "internet-web-browser" )                   iconIndex = L"#0"; // TODO
    else if( id == "multimedia-volume-control" )              iconIndex = L"#0"; // TODO
    else if( id == "office-calendar" )                        iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-accessibility" )      iconIndex = L"#268";
    else if( id == "preferences-desktop-font" )               iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-keyboard-shortcuts" ) iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-locale" )             iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-remote-desktop" )     iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-screensaver" )        iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-sound" )              iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-theme" )              iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop-wallpaper" )          iconIndex = L"#0"; // TODO
    else if( id == "preferences-system-network-proxy" )       iconIndex = L"#0"; // TODO
    else if( id == "preferences-system-session" )             iconIndex = L"#0"; // TODO
    else if( id == "preferences-system-windows" )             iconIndex = L"#0"; // TODO
    else if( id == "system-file-manager" )                    iconIndex = L"#46";
    else if( id == "system-installer" )                       iconIndex = L"#271"; // non standard
    else if( id == "system-software-update" )                 iconIndex = L"#47";
    else if( id == "system-update" )                          iconIndex = L"#47";  // non standard
    else if( id == "system-users" )                           iconIndex = L"#269";
    else if( id == "utilities-system-monitor" )             { iconIndex = L"#107"; file = L"taskmgr.exe"; }
    else if( id == "utilities-terminal" )                   { iconIndex = L"IDI_APPICON"; file = L"cmd.exe"; }
// categories
    else if( id == "applications-accessories" )               iconIndex = L"#0"; // TODO
    else if( id == "applications-development" )               iconIndex = L"#0"; // TODO
    else if( id == "applications-games" )                     iconIndex = L"#0"; // TODO
    else if( id == "applications-graphics" )                  iconIndex = L"#0"; // TODO
    else if( id == "applications-internet" )                  iconIndex = L"#14";
    else if( id == "applications-office" )                    iconIndex = L"#0"; // TODO
    else if( id == "applications-other" )                     iconIndex = L"#0"; // TODO
    else if( id == "applications-system" )                    iconIndex = L"#0"; // TODO
    else if( id == "applications-utilities" )                 iconIndex = L"#0"; // TODO
    else if( id == "preferences-desktop" )                    iconIndex = L"#22";
    else if( id == "preferences-desktop-peripherals" )        iconIndex = L"#0"; // TODO
    else if( id == "preferences-system" )                     iconIndex = L"#0"; // TODO
// devices
    else if( id == "audio-card" )                             iconIndex = L"#0"; // TODO
    else if( id == "audio-input-microphone" )                 iconIndex = L"#0"; // TODO
    else if( id == "battery" )                                iconIndex = L"#0"; // TODO
    else if( id == "camera-photo" )                           iconIndex = L"#0"; // TODO
    else if( id == "computer" )                               iconIndex = L"#0"; // TODO
    else if( id == "drive-harddisk" )                         iconIndex = L"#0"; // TODO
    else if( id == "drive-optical" )                          iconIndex = L"#0"; // TODO
    else if( id == "drive-removable-media" )                  iconIndex = L"#0"; // TODO
    else if( id == "input-gaming" )                           iconIndex = L"#0"; // TODO
    else if( id == "input-keyboard" )                         iconIndex = L"#0"; // TODO
    else if( id == "input-mouse" )                            iconIndex = L"#0"; // TODO
    else if( id == "media-flash" )                            iconIndex = L"#0"; // TODO
    else if( id == "media-floppy" )                           iconIndex = L"#0"; // TODO
    else if( id == "media-jaz" )                              iconIndex = L"#0"; // TODO
    else if( id == "media-optical" )                          iconIndex = L"#0"; // TODO
    else if( id == "media-zip" )                              iconIndex = L"#0"; // TODO
    else if( id == "modem" )                                  iconIndex = L"#0"; // TODO
    else if( id == "multimedia-player" )                      iconIndex = L"#0"; // TODO
    else if( id == "network-wired" )                          iconIndex = L"#0"; // TODO
    else if( id == "network-wireless" )                       iconIndex = L"#0"; // TODO
    else if( id == "phone" )                                  iconIndex = L"#0"; // TODO
    else if( id == "printer" )                                iconIndex = L"#0"; // TODO
    else if( id == "video-display" )                          iconIndex = L"#0"; // TODO
// emblems
    else if( id == "emblem-readonly" )                        iconIndex = L"#0"; // TODO
    else if( id == "emblem-unreadable" )                      iconIndex = L"#0"; // TODO
// emotes
    else if( id == "face-angel" )                             iconIndex = L"#0"; // TODO
    else if( id == "face-cool" )                              iconIndex = L"#0"; // TODO
    else if( id == "face-crying" )                            iconIndex = L"#0"; // TODO
    else if( id == "face-embarassed" )                        iconIndex = L"#0"; // TODO
    else if( id == "face-kiss" )                              iconIndex = L"#0"; // TODO
    else if( id == "face-monkey" )                            iconIndex = L"#0"; // TODO
    else if( id == "face-plain" )                             iconIndex = L"#0"; // TODO
    else if( id == "face-raspberry" )                         iconIndex = L"#0"; // TODO
    else if( id == "face-sad" )                               iconIndex = L"#0"; // TODO
    else if( id == "face-smile" )                             iconIndex = L"#0"; // TODO
    else if( id == "face-smile-big" )                         iconIndex = L"#0"; // TODO
    else if( id == "face-surprise" )                          iconIndex = L"#0"; // TODO
    else if( id == "face-wink" )                              iconIndex = L"#0"; // TODO
// mimetypes
    else if( id == "application-certificate" )              { iconIndex = L"#218"; file = L"certmgr"; }
    else if( id == "application-x-executable" )               iconIndex = L"#0"; // TODO
    else if( id == "audio-x-generic" )                        iconIndex = L"#0"; // TODO
    else if( id == "font-x-generic" )                         iconIndex = L"#0"; // TODO
    else if( id == "image-x-generic" )                        iconIndex = L"#0"; // TODO
    else if( id == "package-x-generic" )                      iconIndex = L"#0"; // TODO
    else if( id == "text-html" )                              iconIndex = L"#0"; // TODO
    else if( id == "text-x-generic" )                         iconIndex = L"#0"; // TODO
    else if( id == "text-x-preview" )                         iconIndex = L"#0"; // TODO
    else if( id == "text-x-generic-template" )                iconIndex = L"#0"; // TODO
    else if( id == "text-x-script" )                          iconIndex = L"#0"; // TODO
    else if( id == "video-x-generic" )                        iconIndex = L"#0"; // TODO
    else if( id == "x-office-address-book" )                  iconIndex = L"#0"; // TODO
    else if( id == "x-office-calendar" )                      iconIndex = L"#0"; // TODO
    else if( id == "x-office-document" )                      iconIndex = L"#0"; // TODO
    else if( id == "x-office-document-template" )             iconIndex = L"#0"; // TODO
    else if( id == "x-office-drawing" )                       iconIndex = L"#0"; // TODO
    else if( id == "x-office-drawing-template" )              iconIndex = L"#0"; // TODO
    else if( id == "x-office-presentation" )                  iconIndex = L"#0"; // TODO
    else if( id == "x-office-presentation-template" )         iconIndex = L"#0"; // TODO
    else if( id == "x-office-spreadsheet" )                   iconIndex = L"#0"; // TODO
    else if( id == "x-office-spreadsheet-template" )          iconIndex = L"#0"; // TODO
// places
    else if( id == "folder" )                                 iconIndex = L"#0"; // TODO
    else if( id == "folder-remote-ftp" )                      iconIndex = L"#0"; // TODO
    else if( id == "folder-remote-nfs" )                      iconIndex = L"#0"; // TODO
    else if( id == "folder-remote-smb" )                      iconIndex = L"#0"; // TODO
    else if( id == "folder-remote-ssh" )                      iconIndex = L"#0"; // TODO
    else if( id == "folder-remote" )                          iconIndex = L"#0"; // TODO
    else if( id == "folder-saved-search" )                    iconIndex = L"#0"; // TODO
    else if( id == "mail-folder-sent" )                       iconIndex = L"#0"; // TODO
    else if( id == "mail-inbox" )                             iconIndex = L"#0"; // TODO
    else if( id == "mail-outbox" )                            iconIndex = L"#0"; // TODO
    else if( id == "network-server" )                         iconIndex = L"#0"; // TODO
    else if( id == "network-workgroup" )                      iconIndex = L"#0"; // TODO
    else if( id == "start-here" )                             iconIndex = L"#0"; // TODO
    else if( id == "user-desktop" )                           iconIndex = L"#0"; // TODO
    else if( id == "user-home" )                              iconIndex = L"#0"; // TODO
    else if( id == "user-trash" )                             iconIndex = L"#0"; // TODO
// status
    else if( id == "appointment-missed" )                     iconIndex = L"#0"; // TODO
    else if( id == "appointment-soon" )                       iconIndex = L"#0"; // TODO
    else if( id == "audio-volume-high" )                      iconIndex = L"#0"; // TODO
    else if( id == "audio-volume-low" )                       iconIndex = L"#0"; // TODO
    else if( id == "audio-volume-medium" )                    iconIndex = L"#0"; // TODO
    else if( id == "audio-volume-muted" )                     iconIndex = L"#0"; // TODO
    else if( id == "dialog-error" )                           iconIndex = L"#0"; // TODO
    else if( id == "dialog-information" )                     iconIndex = L"#0"; // TODO
    else if( id == "dialog-password" )                        iconIndex = L"#0"; // TODO
    else if( id == "dialog-question" )                        iconIndex = L"#0"; // TODO
    else if( id == "dialog-warning" )                         iconIndex = L"#0"; // TODO
    else if( id == "folder-drag-accept" )                     iconIndex = L"#0"; // TODO
    else if( id == "folder-open" )                            iconIndex = L"#0"; // TODO
    else if( id == "folder-visiting" )                        iconIndex = L"#0"; // TODO
    else if( id == "image-loading" )                          iconIndex = L"#0"; // TODO
    else if( id == "image-missing" )                          iconIndex = L"#0"; // TODO
    else if( id == "mail-attachment" )                        iconIndex = L"#0"; // TODO
    else if( id == "mail-read" )                              iconIndex = L"#0"; // TODO
    else if( id == "mail-replied" )                           iconIndex = L"#0"; // TODO
    else if( id == "mail-unread" )                            iconIndex = L"#0"; // TODO
    else if( id == "media-playlist-repeat" )                  iconIndex = L"#0"; // TODO
    else if( id == "media-playlist-shuffle" )                 iconIndex = L"#0"; // TODO
    else if( id == "network-error" )                        { iconIndex = L"#196"; file = L"netshell"; }
    else if( id == "network-idle" )                         { iconIndex = L"#193"; file = L"netshell"; }
    else if( id == "network-offline" )                      { iconIndex = L"#195"; file = L"netshell"; }
    else if( id == "network-receive" )                      { iconIndex = L"#192"; file = L"netshell"; }
    else if( id == "network-transmit" )                     { iconIndex = L"#191"; file = L"netshell"; }
    else if( id == "network-transmit-receive" )             { iconIndex = L"#190"; file = L"netshell"; }
    else if( id == "network-wireless-encrypted" )             iconIndex = L"#0"; // TODO
    else if( id == "network-wireless-high" )                { iconIndex = L"#23515"; file = L"netshell"; }
    else if( id == "network-wireless-low" )                 { iconIndex = L"#23510"; file = L"netshell"; }
    else if( id == "network-wireless-medium" )              { iconIndex = L"#23513"; file = L"netshell"; }
    else if( id == "security-high" )                          iconIndex = L"#0"; // TODO
    else if( id == "security-low" )                           iconIndex = L"#0"; // TODO
    else if( id == "security-medium" )                        iconIndex = L"#0"; // TODO
    else if( id == "user-trash-full" )                        iconIndex = L"#0"; // TODO
    else if( id == "weather-clear" )                          iconIndex = L"#0"; // TODO
    else if( id == "weather-clear-night" )                    iconIndex = L"#0"; // TODO
    else if( id == "weather-few-clouds" )                     iconIndex = L"#0"; // TODO
    else if( id == "weather-few-clouds-night" )               iconIndex = L"#0"; // TODO
    else if( id == "weather-fog" )                            iconIndex = L"#0"; // TODO
    else if( id == "weather-overcast" )                       iconIndex = L"#0"; // TODO
    else if( id == "weather-showers" )                        iconIndex = L"#0"; // TODO
    else if( id == "weather-snow" )                           iconIndex = L"#0"; // TODO
    else if( id == "weather-storm" )                          iconIndex = L"#0"; // TODO

    return GetShellIcon( iconIndex, width, height, file );
}

}}} // namespace gui::detail::win32

#endif // GUI_WIN32_DETAIL_SHELL_HPP
