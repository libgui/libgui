/*
    Name:        gui/win32/detail/register.hpp
    Purpose:     Win32 class registration
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/15
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_DETAIL_REGISTER_HPP
#define GUI_WIN32_DETAIL_REGISTER_HPP

#include "gui/win32/detail/wndproc.hpp"

static bool register_class( const wchar_t *class_name, size_t class_size )
{
    WNDCLASSEXW wc;
    ::ZeroMemory( &wc, sizeof( WNDCLASSEXW ) );
    wc.cbSize        = sizeof( WNDCLASSEXW );
    wc.style         = 0;
    wc.lpfnWndProc   = ::WindowProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = class_size;
    wc.hInstance     = ::GetModuleHandleW(NULL);
    wc.hIcon         = ::LoadIconW(NULL, IDI_APPLICATION);
    wc.hCursor       = ::LoadCursorW(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = class_name;
    wc.hIconSm       = ::LoadIconW(NULL, IDI_APPLICATION);

    return ::RegisterClassExW(&wc);
}

#endif // GUI_WIN32_DETAIL_REGISTER_HPP
