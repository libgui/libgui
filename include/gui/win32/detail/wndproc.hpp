/*
    Name:        gui/win32/detail/wndproc.hpp
    Purpose:     Win32 WindowProc
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/14
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_DETAIL_WNDPROC_HPP
#define GUI_WIN32_DETAIL_WNDPROC_HPP

#include "gui/menu.hpp"
#include "gui/menuitem.hpp"
#include "gui/widget.hpp"
#include "gui/toplevel.hpp"

LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    using namespace gui;

    widget<win32> *w = widget_from_native(hWnd);

    if( w )
    {
        if( msg == WM_CLOSE )
        {
            toplevel<win32> *tlw = dynamic_cast< toplevel<win32> * >( w );
            if( tlw )
                tlw->close();

            return 0;
        }
        else if( msg == WM_COMMAND )
        {
            WORD id  = LOWORD(wParam);
            WORD cmd = HIWORD(wParam);
            HWND ctl = (HWND) lParam;
            if( ctl )
            {
                widget<win32> *c = widget_from_native(ctl);
                if( c )
                    c->sig_select(c);
            }
            return 0;
        }
        else if( msg == WM_DESTROY )
        {
            return 0;
        }/*
        else if( msg == WM_CONTEXTMENU  )
        {
            std::cout << "WM_CONTEXTMENU\n";
        }
        else if( msg == WM_MENUSELECT  )
        {
            std::cout << "WM_MENUSELECT\n";
        }*/
        else if( msg == WM_MENUCOMMAND )
        {
            UINT  idx = (UINT) wParam; // global index, used as menuitem id
            HMENU ctl = (HMENU)lParam; // handle to the menuitem's owner 
            if( ctl )
            {
                menu<win32> *m = menu_from_native(ctl);
                if( m )
                {
                    menuitem<win32> *item = m->item_by_id(idx);
                    if( item )
                        item->sig_select(item);
                }
            }
            return 0;
        }
        else if( msg == WM_PAINT )
        {
            PAINTSTRUCT ps;
            HDC         hDC;
            hDC = ::BeginPaint(hWnd, &ps);

            gfx::context<win32> ctx(hDC);
            w->sig_paint(ctx);

            ::EndPaint(hWnd, &ps);
            return 0;
        }
        else if( msg == WM_SIZE )
        {
            // TODO: fix statusbar resize, maybe connecting it's own sig_size.
            ::SendMessageW( ::GetDlgItem(hWnd, 999), WM_SIZE, 0, 0);
            return 0;
        }
    }

    return ::DefWindowProcW( hWnd, msg, wParam, lParam );
}

#endif // GUI_WIN32_DETAIL_WNDPROC_HPP
