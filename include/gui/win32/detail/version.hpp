/*
    Name:        gui/win32/detail/version.hpp
    Purpose:     Retrieve the comctl32.dll version at runtime
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/25
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_DETAIL_VERSION_HPP
#define GUI_WIN32_DETAIL_VERSION_HPP

namespace gui { namespace detail { namespace win32 {

// http://www.geoffchappell.com/studies/windows/shell/comctl32/api/index.htm
// TODO: Tested on XP Pro SP3 and 7 Home Pro

static int ComCtl32Version()
{
    static int comCtl32Ver = 0;
    if( !comCtl32Ver )
    {
        HMODULE module = ::GetModuleHandleW(L"COMCTL32");
        if( module )
        {
            FARPROC proc = ::GetProcAddress( module, "HIMAGELIST_QueryInterface" );
            if( !proc )
                comCtl32Ver = 581; // not found, must be < 6.00
            else
            {
                proc = ::GetProcAddress( module, "ImageList_Resize" );
                if( !proc )
                    comCtl32Ver = 600; // not found, must be < 6.10
                else
                    comCtl32Ver = 610; // found, must be >= 6.10
            }
        }
    }
    return comCtl32Ver;
}

}}} // namespace gui::detail::win32

#endif // GUI_WIN32_DETAIL_VERSION_HPP
