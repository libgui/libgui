/*
    Name:        gui/win32/menu.ipp
    Purpose:     Win32 menu implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_MENU_IPP
#define GUI_WIN32_MENU_IPP

#include <nowide/convert.hpp>

namespace gui {

typedef std::vector< menu<win32> * > menu_vector;
menu_vector menus;

menu<win32> *menu_from_native( native_menu_t hMenu )
{
    for( menu_vector::iterator it = menus.begin();
                              it != menus.end(); ++it )
        if( (*it)->native() == hMenu )
            return (*it);

    return nullptr;
}

template <typename T>
menu<T>::menu()
{
    m_native = ::CreatePopupMenu();
    menus.push_back( this );
}

template <typename T>
menu<T>::~menu()
{
    m_items.clear();
    m_submenus.clear();
}

template <typename T>
void menu<T>::add_submenu( menu<T> *menu, const std::string &title )
{
    if( !menu )
        return;

    MENUITEMINFOW mi;

    std::wstring  wide = nowide::widen( title );
    wchar_t      *text = const_cast<wchar_t *>( wide.c_str() );
    size_t       index = m_items.size() + m_submenus.size();

    mi.cbSize     = sizeof( MENUITEMINFOW );
    mi.fMask      = MIIM_STRING | MIIM_ID | MIIM_SUBMENU;
    mi.dwTypeData = text;
    mi.wID        = index;
    mi.hSubMenu   = menu->native();

    if( !::InsertMenuItemW(m_native, index, TRUE, &mi) )
    {
        DWORD lastError = GetLastError();
        std::cerr << "add_submenu error: "<< lastError << '\n';
        return;
    }

    m_submenus.push_back( menu );
}

template <typename T>
void menu<T>::add_item( menuitem<T> *item )
{
    if( !item )
        return;

    MENUITEMINFOW mi;

    std::wstring  wide = nowide::widen( item->label() );
    wchar_t      *text = const_cast<wchar_t *>( wide.c_str() );
    size_t       index = m_items.size() + m_submenus.size();

    mi.cbSize     = sizeof( MENUITEMINFOW );
    mi.fMask      = MIIM_STRING | MIIM_ID;
    mi.dwTypeData = text;
    mi.wID        = index;

    if( !::InsertMenuItemW(m_native, index, TRUE, &mi) )
    {
        DWORD lastError = GetLastError();
        std::cerr << "add_item error: "<< lastError << '\n';
        return;
    }

    item->m_native = &mi;

    m_items.push_back( item );
}

template <typename T>
menuitem<T> *menu<T>::item_at( size_t index )
{
    if( index >= m_items.size() )
        return nullptr;

    return m_items[index];
}

template <typename T>
menuitem<T> *menu<T>::item_by_id( int id )
{
    for( typename menuitems::iterator it = m_items.begin();
                                     it != m_items.end(); ++it )
    {
        if( (*it)->id() == id )
            return (*it);
    }

    return nullptr;
}

} // namespace gui

#endif // GUI_WIN32_MENU_IPP
