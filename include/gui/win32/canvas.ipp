/*
    Name:        gui/win32/canvas.ipp
    Purpose:     Win32 canvas implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/21
    Revision:    $ID$
    Licence:     ???
*/
#ifndef GUI_WIN32_CANVAS_IPP
#define GUI_WIN32_CANVAS_IPP

#include <iostream>
#include "gui/win32/detail/register.hpp"

namespace gui {

template <typename T>
canvas<T>::canvas( widget<T> *parent )
{
    ::register_class( L"canvas", sizeof( canvas<win32> * ) );

    // TODO: remove this once layout management is done.
    RECT parentRect;
    GetClientRect(parent->native(), &parentRect); 

    this->m_native = CreateWindowExW(0,
                                    L"canvas",
                                    NULL,
                                    WS_CHILD | WS_VISIBLE ,
                                    parentRect.top, parentRect.left,
                                    parentRect.right, parentRect.bottom,
/*
                                    CW_USEDEFAULT, CW_USEDEFAULT,
                                    CW_USEDEFAULT, CW_USEDEFAULT,
*/
                                    parent->native(),
                                    NULL,
                                    GetModuleHandleW(NULL), NULL);
    if( !this->m_native )
    {
        DWORD lastError = GetLastError();
        std::cerr << "Error creating the canvas: "<< GetLastError() << '\n';
    }
}

template <typename T>
canvas<T>::~canvas()
{
}

} // namespace gui

#endif // GUI_WIN32_CANVAS_IPP
