/*
    Name:        gui/win32/menuitem.ipp
    Purpose:     Win32 menuitem implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_MENUITEM_IPP
#define GUI_WIN32_MENUITEM_IPP

namespace gui {

template <typename T>
menuitem<T>::menuitem( const std::string &label )
:
m_label(label),
m_native(NULL)
{
}

template <typename T>
menuitem<T>::~menuitem()
{
}

template <typename T>
int menuitem<T>::id() const
{
    if( m_native )
        return m_native->wID;

    return -1;
}

} // namespace gui

#endif // GUI_WIN32_MENUITEM_IPP
