/*
    Name:        gui/win32/messagedialog.ipp
    Purpose:     Win32 message dialog implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/16
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_MESSAGEDIALOG_HPP
#define GUI_WIN32_MESSAGEDIALOG_HPP

#include <nowide/convert.hpp>

namespace gui {

template <typename T>
messagedialog<T>::messagedialog(toplevel<T> *parent,
                                const std::string &message,
                                const std::string &title,
                                int id,
                                msg_t msgtype)
:   m_message(message),
    m_title(title),
    m_id(id),
    m_msgtype(msgtype)
{
    this->m_parent = parent;
}

template <typename T>
messagedialog<T>::~messagedialog()
{
}

template <typename T>
int messagedialog<T>::show_modal()
{
    size_t id = 0;

    if( m_id & (id_yes|id_no) )
    {
        if( m_id & id_cancel )
            id = MB_YESNOCANCEL;
        else
            id = MB_YESNO;
    }
    else if( m_id & (id_retry|id_ignore) )
    {
        if( m_id & id_abort )
            id = MB_ABORTRETRYIGNORE;
        else if( m_id & id_cancel )
            id = MB_CANCELTRYCONTINUE;
    }
    else if( m_id & id_cancel )
    {
        if( m_id & id_ok )
            id = MB_OKCANCEL;
        else if( m_id & id_retry )
            id = MB_RETRYCANCEL;
    }
    else if( m_id & id_ok )
    {
        id = MB_OK;
    }

    if( m_id & id_help )
        id |= MB_HELP;

    id |= MB_USERICON;

    wchar_t *icon = NULL;

    switch( m_msgtype )
    {
    case msg_error:
        icon = IDI_ERROR;
        break;
    case msg_question:
        icon = IDI_QUESTION;
        break;
    case msg_warning:
        icon = IDI_WARNING;
        break;
    case msg_info:
        icon = IDI_INFORMATION;
        break;
#if _WIN32_WINNT >= 0x0600
    case msg_security:
        icon = IDI_SHIELD;
        break;
#endif
    default:
        icon = IDI_INFORMATION;
    }

    std::wstring sMessage  = nowide::widen(m_message);
    std::wstring sCaption  = nowide::widen(m_title);
    const wchar_t *text    = const_cast<wchar_t *>(sMessage.c_str());
    const wchar_t *caption = const_cast<wchar_t *>(sCaption.c_str());

    MSGBOXPARAMSW params;
    ::ZeroMemory( &params, sizeof( MSGBOXPARAMSW ) );
    params.cbSize       = sizeof( MSGBOXPARAMSW );
    params.hInstance    = NULL;
    params.lpszText     = text;
    params.lpszCaption  = caption;
    params.dwStyle      = id;
    params.lpszIcon     = icon;
    params.dwLanguageId = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);

    if( this->m_parent )
        params.hwndOwner = this->m_parent->native();

    int response = ::MessageBoxIndirectW(&params);

    switch( response )
    {
        case IDOK:      return id_ok;
        case IDCANCEL:  return id_cancel;
        case IDABORT:   return id_abort;
        case IDRETRY:   return id_retry;
        case IDIGNORE:  return id_ignore;
        case IDYES:     return id_yes;
        case IDNO:      return id_no;
        case IDCLOSE:   return id_close;
        default:        return id_none;
    }
}

} // namespace gui

#endif // GUI_WIN32_MESSAGEDIALOG_HPP
