/*
    Name:        gui/win32/config.hpp
    Purpose:     GTK config
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/14
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_CONFIG_HPP
#define GUI_WIN32_CONFIG_HPP

// Exclude APIs such as Cryptography, DDE, RPC, Shell, and Windows Sockets.
#define WIN32_LEAN_AND_MEAN

#ifdef _MSC_VER
    // Not using WINMAIN
    #pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

// At least Windows XP
#ifndef _WIN32_WINNT
    #define _WIN32_WINNT 0x0501
#endif

// At least IE 7.0
#ifndef _WIN32_IE
    #define _WIN32_IE 0x0700
#endif

#include <windows.h>
#include <commctrl.h>

namespace gui {

    struct  win32 {};

    typedef win32           native_backend_t;
    typedef HWND            native_widget_t;
    typedef HMENU           native_menu_t;
    typedef LPMENUITEMINFOW native_menuitem_t;
    typedef HDC             native_gc_t;

} // namespace gui

#endif // GUI_WIN32_CONFIG_HPP
