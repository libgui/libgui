/*
    Name:        gui/win32/dialog.ipp
    Purpose:     Win32 dialog implementation
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/16
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIN32_DIALOG_IPP
#define GUI_WIN32_DIALOG_IPP

namespace gui {

template <typename T>
dialog<T>::dialog()
{
}

template <typename T>
dialog<T>::~dialog()
{
}

} // namespace gui

#endif // GUI_WIN32_DIALOG_IPP
