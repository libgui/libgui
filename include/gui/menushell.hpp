/*
    Name:        gui/gtk/menushell.hpp
    Purpose:     GTK menu shell interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/07
    Licence:     Boost Software License, Version 1.0
*/

#ifndef GUI_MENUSHELL_HPP
#define GUI_MENUSHELL_HPP

#include "gui/menuitem.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class menushell

    Interface for menu based classes.
    @todo Actually unused.
*/
class menushell
{
public:
/** Virtual destructor */
    virtual ~menushell();

    void push_back ( menuitem<T> *item );

    void push_front( menuitem<T> *item );

    void insert    ( int position,
                     menuitem<T> *item );

    native_menu_t native() const { return m_menu; }

protected:
    menushell();

    native_menu_t m_menu;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/menushell.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/menushell.ipp"
#endif

#endif // GUI_MENUSHELL_HPP
