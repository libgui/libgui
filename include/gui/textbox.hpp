/*
    Name:        gui/textbox.hpp
    Purpose:     Text box interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_TEXTBOX_HPP
#define GUI_TEXTBOX_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class textbox

    Text box.
*/
class textbox : public widget<T>
{
public:
//  No child widgets without parent.
    textbox() = delete;
/**
    Constructor.
    @param parent The parent widget.
*/
    textbox( widget<T> *parent );

/** Virtual destructor. */
    virtual ~textbox();
/**
    Sets the text to display inside the box.
    @param text Text to display.
*/
    void set_text( const std::string &text );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/textbox.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/textbox.ipp"
#endif

#endif // GUI_TEXTBOX_HPP
