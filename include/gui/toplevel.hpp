/*
    Name:        gui/toplevel.h
    Purpose:     Toplevel window interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/20
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_TOPLEVEL_HPP
#define GUI_TOPLEVEL_HPP

#include <string>
#include <boost/signals2/signal.hpp>
#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class toplevel

    A toplevel widget container interface.
    All concrete window/dialog classes should derive from this interface.
*/
class toplevel : public widget<T>
{
public:
/** Virtual destructor. */
    virtual ~toplevel();
/**
    Emits a signal to a window to be deleted, with the possibility to
    vetoing the action.

    An example is when the window close button was pressed and we need
    a quit confirmation dialog to be shown: pressing a cancel button
    the command is ignored.
*/
    void close();
/**
    Sets the window bar title.
    @param title The window bar title.
*/
    void set_title( const std::string &title );
/**
    Returns the window bar title.
    @return The window bar title.
*/
    std::string title() const;

/** Close signal. */
    boost::signals2::signal< bool( toplevel<T> *tlw ) > sig_close;

protected:
    toplevel( toplevel<T> *parent );
    toplevel() : toplevel(nullptr) {}
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/toplevel.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/toplevel.ipp"
#endif

#endif // GUI_TOPLEVEL_HPP
