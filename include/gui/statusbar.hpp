/*
    Name:        gui/statusbar.hpp
    Purpose:     Menubar interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/08
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_STATUSBAR_HPP
#define GUI_STATUSBAR_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class statusbar

    Status bar.
*/
class statusbar : public widget<T>
{
public:
//  No child widgets without parent.
    statusbar() = delete;
/**
    Constructor.
    @param parent The parent widget.
*/
    statusbar( widget<T> *parent );

/** Destructor. */
    ~statusbar();
/**
    Sets the text to display inside the status bar.
    @param text Text to display.
*/
    void set_text( const std::string &text );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/statusbar.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/statusbar.ipp"
#endif

#endif // GUI_STATUSBAR_HPP
