/*
    Name:        gui/dialog.hpp
    Purpose:     Dialog interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/16
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_DIALOG_HPP
#define GUI_DIALOG_HPP

#include "gui/toplevel.hpp"

namespace gui {

/** Response id. */
enum response_id
{
    id_none     = 0,    /**< No/undefined response. */
    id_ok       = 1,    /**< Generic agreement. */
    id_cancel   = 2,    /**< Cancel operation. */
    id_abort    = 4,    /**< Abort operation. */
    id_retry    = 8,    /**< Retry operation. */
    id_ignore   = 16,   /**< Ignore sentence. */
    id_yes      = 32,   /**< Accept request. */
    id_no       = 64,   /**< Decline request. */
    id_close    = 128,  /**< Close the dialog/window. */
    id_apply    = 256,  /**< Apply changes. */
    id_help     = 512   /**< Request context help. */
};

template <typename T = native_backend_t>
/**
    @class dialog

    Dialog window.
*/
class dialog : public toplevel<T>
{
public:
/** Constructor */
    dialog();
/**
    Constructor.
    @param parent The parent window.
*/
    dialog( toplevel<T> *parent );

/** Virtual destructor. */
    virtual ~dialog();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/dialog.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/dialog.ipp"
#endif

#endif // GUI_DIALOG_HPP
