

#ifndef GUI_TRAITS_HPP
#define GUI_TRAITS_HPP

#include "gui/config.hpp"
#include "gui/toplevel.hpp"

namespace gui {

//template< typename T > toplevel_tag {}


template< typename T, typename B = native_backend_t >
bool is_toplevel( T* )
{
    return false;
}

template< typename B >
bool is_toplevel( toplevel<B>* )
{
    return true;
}

} // namespace gui

#endif // GUI_TRAITS_HPP
