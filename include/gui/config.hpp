/*
    Name:        gui/config.hpp
    Purpose:     global config
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/05/05
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_CONFIG_HPP
#define GUI_CONFIG_HPP

#include <gui/detail/platform.hpp>

// HAVE macros are defined by the build system and/or by platform.hpp
// to specify that the selected backend is available
// e.g. using pkgconfig to find gtk3 headers/libs.

// NEED macros are defined by the user
// to choose the desired gui backend to be used in his own application,
// so it could be also e.g. GTK3 on Win32.

#if defined(GUI_NEED_COCOA) && defined(GUI_HAVE_COCOA)
#   define GUI_USE_COCOA 1
#   include <gui/cocoa/config.hpp>
#elif defined(GUI_NEED_GTK) && defined(GUI_HAVE_GTK)
#   define GUI_USE_GTK 1
#   include <gui/gtk/config.hpp>
#elif defined(GUI_NEED_WIN32) && defined(GUI_HAVE_WIN32)
#   define GUI_USE_WIN32 1
#   include <gui/win32/config.hpp>
#else
#   error "Port not available."
#endif

#if (__cplusplus >= 201103L )
#   define GUI_HAVE_CPP11
#endif

#endif // GUI_CONFIG_HPP
