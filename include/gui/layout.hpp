/*
    Name:        gui/layout.hpp
    Purpose:     Layout interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/30
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_LAYOUT_HPP
#define GUI_LAYOUT_HPP

#include "gui/config.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class layout

    Interface for resizing classes.
*/
class layout
{
public:
    ~layout() {}

protected:
    layout() {}
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/layout.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/layout.ipp"
#endif

#endif // GUI_LAYOUT_HPP
