/*
    Name:        gui/gfx/context.hpp
    Purpose:     Drawing context class
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/09
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GFX_CONTEXT_HPP
#define GUI_GFX_CONTEXT_HPP

namespace gui {
namespace gfx {

template <typename T = native_backend_t>

class context
{
public:
    context();
    context(native_gc_t ctx);
    virtual ~context();

    void line_to(double x, double y);
    void move_to(double x, double y);
    void stroke();

    void set_source_rgba(double r, double g, double b, double a);
    void set_line_width(double width);

    native_gc_t native() const { return m_context; }

protected:
    native_gc_t m_context;

};// class context
} // namespace gfx;
} // namespace gui;

#if GUI_USE_GTK
    #include "gui/gtk/gfx/context.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/gfx/context.ipp"
#endif

#endif // GUI_GFX_CONTEXT_HPP
