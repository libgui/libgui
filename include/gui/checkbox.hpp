/*
    Name:        gui/checkbox.hpp
    Purpose:     Check box widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_CHECKBOX_HPP
#define GUI_CHECKBOX_HPP

#include <string>
#include "gui/config.hpp"
#include "gui/pushable.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class checkbox

    Check box.
*/
class checkbox : public pushable<T>
{
public:
/** Constructor. */
    checkbox() {}
/**
    Constructor.
    @param parent The parent widget.
*/
    checkbox( widget<T> *parent );

/** Destructor. */
    ~checkbox();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/checkbox.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/checkbox.ipp"
#endif

#endif // GUI_CHECKBOX_HPP
