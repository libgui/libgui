/*
    Name:        gui/widget.hpp
    Purpose:     Widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/19
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_WIDGET_HPP
#define GUI_WIDGET_HPP

#include "gui/geometry.hpp"

#include <boost/signals2/signal.hpp>

namespace gui {

namespace gfx { template<typename T> class context; }

template <typename T = native_backend_t>
/**
    @class widget

    Interface, common to most of, if not all, widgets.
*/
class widget
{
public:
/** Constructor */
    widget();
/**
    Constructor.
    @param native The native widget.
*/
    widget( native_widget_t native );
/**
    Constructor.
    @param parent The parent widget.
*/
    widget( widget<T> *parent );

/** Virtual destructor */
    virtual ~widget();

/** Adds a child widget. @todo Replace/delete this function */
    virtual void add( widget<T> *child );

/** Destroys the widget. */
    void destroy();

/** Enable the widget. */
    void enable() { do_enable( true ); }

/** Disable the widget. */
    void disable()  { do_enable( false ); }
/**
    Returns if the widget is accessible.
    @return @true if the widget is accessible, @false otherwise.

    To retrieve if the widget is set to be enabled, use is_set_enabled() instead.
    @see is_set_enabled()
*/
    bool is_enabled() const
    {
        return
        m_enabled && ( m_isToplevel || !m_parent || m_parent->is_enabled() );
    }
/**
    Returns if the widget is set to be enabled,
    without taking into account the real accessibility state,
    e.g. a parent widget is disabled, including this child.
    @return @true if the widget is set to be enabled, @false otherwise.

    To retrieve the accessibility state use is_enabled() instead.

    @see is_enabled()
*/
    bool is_set_enabled() const { return m_enabled; }

/** Expand the size of the widget using the specified orientation */
    void expand( orientation_t orient );
/**
    Returns the widget height.
    @return The widget height.
*/
    int height() const;

/** Makes the widget invisible. */
    void hide();
/**
    Returns the widget width.
    @return The widget width.
*/
    int width() const;
/**
    Sets the size of this widget.
    @param width  New width.
    @param height New height.
*/
    void set_size( int width, int height ) { do_set_size( width, height ); }

/** Make visible a widget. */
    void show();

/** Show recursively a widget and all its children. */
    void show_all();
/**
    Returns the native widget handle.
    @return The native widget handle.
*/
    native_widget_t native() const { return m_native; }
/**
    Returns the parent widget.
    @return The parent widget.
*/
    widget<T> *parent() const { return m_parent; }

/** Post construction. */
    void post_creation();
/**
    Set ID.
    @param id ID.
*/
    void set_id( int id ) { m_id = id; }
/**
    Get ID.
    @return ID or empty.
*/
    int id() const { return m_id; }
/**
    The widget is selected with some pointing device like
    a mouse or user gesture.
*/
    boost::signals2::signal<void(widget<T> *)> sig_select;
/**
    The widget is marked for destruction and
    the user can perform some clean up.
*/
    boost::signals2::signal<void(widget<T> *)> sig_destroy;
/**
    The widget is being redrawn and
    the user can perform some custom drawing operation.
*/
    boost::signals2::signal<void(gfx::context<T> &)> sig_paint;

// Reminder:
// sig_release, sig_hover, sig_leave, sig_focus and sig_focus_lost

protected:
    virtual void do_set_size( int width, int height );

    native_widget_t m_native;
    widget<T>      *m_parent;
    bool            m_isToplevel;
    int             m_id;

private:
    virtual void do_native() {}

    void do_enable( bool enable );
    bool m_enabled;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/widget.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/widget.ipp"
#endif

#endif // GUI_WIDGET_HPP
