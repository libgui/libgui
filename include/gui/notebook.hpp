/*
    Name:        gui/notebook.hpp
    Purpose:     Notebook widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/03
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_NOTEBOOK_HPP
#define GUI_NOTEBOOK_HPP

#include "gui/config.hpp"
#include "gui/bookshell.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class notebook

    Notebook
*/
class notebook : public bookshell<T>
{
public:
//  No child widgets without parent.
    notebook() = delete;
/**
    Constructor.
    @param parent The parent widget.
*/
    notebook( widget<T> *parent );

/** Destructor */
    ~notebook();
/**
    Sets if the tabs will be shown.
    @param show @true if the tabs should be shown, @false otherwise.
*/
    void set_show_tabs  ( bool show = true );
/**
    Sets if the tabs can be scrollable using arrow buttons.
    @param scrollable @true if the tabs can be scrollable, @false otherwise.
*/
    void set_scrollable ( bool scrollable = true );

private:
    int do_insert_page( widget<T> *page, const std::string& label, size_t index );

    void do_set_selection( size_t index );
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/notebook.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/notebook.ipp"
#endif

#endif // GUI_NOTEBOOK_HPP
