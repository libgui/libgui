/*
    Name:        gui/messagedialog.hpp
    Purpose:     Message dialog interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/16
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_MESSAGEDIALOG_HPP
#define GUI_MESSAGEDIALOG_HPP

#include "gui/dialog.hpp"

namespace gui {

/** Notification message type. */
enum msg_t
{
    msg_none = 0, /**< No message. */
    msg_info,     /**< Information message. */
    msg_warning,  /**< Warning message. */
    msg_question, /**< Question message. */
    msg_error,    /**< Error message. */
    msg_security, /**< Security message. */
    msg_other     /**< Not categorized. */
};

template <typename T = native_backend_t>
/**
    @class messagedialog

    A dialog with an image representing the type of message
    (Error, Question, etc.) alongside some message text.
*/
class messagedialog : public dialog<T>
{
public:
//  No child widgets without parent.
    messagedialog() = delete;
/**
    Constructor.
    @param parent  The parent widget.
    @param message The message to display.
    @param title   The dialog title.
    @param id      The button(s) to use.
    @param msgtype The message type.
*/
    messagedialog( toplevel<T> *parent,
                    const std::string &message,
                    const std::string &title = std::string(),
                    int id = id_ok,
                    msg_t msgtype = msg_info );
/** Destructor. */
    ~messagedialog();
/**
    Show as modal dialog.
    @return The response id from the button pressed to close the dialog.
*/
    int show_modal();

private:
    std::string m_message,
                m_title;
    int         m_id;
    msg_t       m_msgtype;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/messagedialog.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/messagedialog.ipp"
#endif

#endif // GUI_MESSAGEDIALOG_HPP
