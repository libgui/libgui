/*
    Name:        gui/menu.hpp
    Purpose:     Menubar interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_MENU_HPP
#define GUI_MENU_HPP

#include <vector>

#include "gui/config.hpp"
#include "gui/menuitem.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class menu

    Menu.
*/
class menu
{
public:
/** Constructor. */
    menu();

/** Destructor */
    ~menu();
/**
    Adds a submenu to the current menu.
    @param child The menu to add.
    @param label The added menu label.
*/
    void add_submenu( menu<T> *child, const std::string &label );
/**
    Adds a menuitem to the current menu.
    @param item The menuitem to add.
*/
    void add_item( menuitem<T> *item );
/**
    Returns the menuitem at the specified index or @nullptr if not found.
    @param index The menuitem index.
    @return The menuitem at the specified index or @nullptr if not found.
*/
    menuitem<T> *item_at( size_t index );
/**
    Returns the menuitem with the specified id or @nullptr if not found.
    @param id The menuitem id.
    @return The menuitem with the specified id or @nullptr if not found.
*/
    menuitem<T> *item_by_id( int id );
/**
    Returns the native menu handle.
    @return Native menu handle.
*/
    native_menu_t native() const { return m_native; }

private:
    native_menu_t m_native;

    typedef std::vector< menuitem<T>* > menuitems;
    std::vector< menu<T>* > m_submenus;
    menuitems               m_items;
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/menu.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/menu.ipp"
#endif

#endif // GUI_MENU_HPP
