/*
    Name:        gui/bookshell.hpp
    Purpose:     Book widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/03
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_BOOKSHELL_HPP
#define GUI_BOOKSHELL_HPP

#include <string>
#include <deque>

#include "gui/widget.hpp"

namespace gui {

enum { not_found = -1 /**< Item not found id. @todo Move it to a more appropriate place. */};

template<typename T>
/**
    @class bookshell

    Interface for book based widgets ( @ref notebook ).
*/
class bookshell : public widget<T>
{
public:
/** Virtual destructor */
    virtual ~bookshell();
/**
    Appends a new page at the end of the book.
    @param page A widget to insert as page.
    @param label Page label.
    @return New page index.
*/
    int append_page( widget<T> *page, const std::string& label );
/**
    Prepends a new page at the beginning of the book.
    @param page A widget to insert as page.
    @param label Page label.
    @return New page index.
*/
    int prepend_page( widget<T> *page, const std::string& label );
/**
    Inserts a new page at given position into the book.
    @param page A widget to insert as page.
    @param label Page label.
    @param index Requested page index.
    @return New page index.
*/
    int insert_page( widget<T> *page, const std::string& label, size_t index );

/** Returns the total number of pages in the book. */
    size_t page_count() const;

/** Returns the current selected page index. */
    int selection() const;

/** Sets the current page index and show it. */
    void set_selection( size_t index );

protected:
    struct page_t
    {
        widget<T>  *page;
        std::string label;

        page_t( widget<T> *page_, std::string label_ )
        :   page(page_),
            label(label_)
        {}

        bool operator==( const page_t& rhs ) const
        { return (page == rhs.page) && (label == rhs.label); }
    };

    typedef std::deque<page_t> pages_t;

    bookshell( widget<T> *parent );

    int         m_selection;
    widget<T>  *m_controller;
    pages_t     m_pages;

private:
    virtual int do_insert_page( widget<T> *page,
                                const std::string& label, size_t index ) = 0;

    virtual void do_set_selection( size_t index ) = 0;
};

} // namespace gui

#include "gui/common/bookshell.ipp"

//#if GUI_USE_GTK
//    #include "gui/gtk/bookshell.ipp"
//#elif GUI_USE_WIN32
//    #include "gui/win32/bookshell.ipp"
//#endif

#endif // GUI_BOOKSHELL_HPP
