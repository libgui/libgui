/*
    Name:        gui/grid.hpp
    Purpose:     Menubar interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/06/06
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_GRID_HPP
#define GUI_GRID_HPP

#include "gui/config.hpp"
#include "gui/widget.hpp"

namespace gui {

template <typename T = native_backend_t>
/**
    @class grid

    Grid layout.
*/
class grid : public widget<T>
{
public:
    grid();
    grid(gui::widget<T> *parent);
    ~grid();

    void attach(const gui::widget<T> &child, int col, int row,
                int colspan, int rowspan);
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/grid.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/grid.ipp"
#endif

#endif // GUI_GRID_HPP
