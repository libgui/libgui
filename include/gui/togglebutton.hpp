/*
    Name:        gui/togglebutton.hpp
    Purpose:     Toggle button widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_TOGGLEBUTTON_HPP
#define GUI_TOGGLEBUTTON_HPP

#include <string>
#include "gui/config.hpp"
#include "gui/pushable.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class togglebutton

    Toggle button.
*/
class togglebutton : public pushable<T>
{
public:
/** Constructor. */
    togglebutton() {}
/**
    Constructor.
    @param parent The parent widget.
*/
    togglebutton( widget<T> *parent );

/** Destructor. */
    ~togglebutton();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/togglebutton.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/togglebutton.ipp"
#endif

#endif // GUI_TOGGLEBUTTON_HPP
