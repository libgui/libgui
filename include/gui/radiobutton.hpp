/*
    Name:        gui/radiobutton.hpp
    Purpose:     Radio button widget interface
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/12/28
    Licence:     Boost Software License, Version 1.0
*/
#ifndef GUI_RADIOBUTTON_HPP
#define GUI_RADIOBUTTON_HPP

#include <string>
#include "gui/config.hpp"
#include "gui/pushable.hpp"

namespace gui {

template<typename T = native_backend_t>
/**
    @class radiobutton

    Radio button.
*/
class radiobutton : public pushable<T>
{
public:
/** Constructor. */
    radiobutton() {}
/**
    Constructor.
    @param parent The parent widget.
*/
    radiobutton( widget<T> *parent );

/** Destructor. */
    ~radiobutton();
};

} // namespace gui

#if GUI_USE_GTK
    #include "gui/gtk/radiobutton.ipp"
#elif GUI_USE_WIN32
    #include "gui/win32/radiobutton.ipp"
#endif

#endif // GUI_RADIOBUTTON_HPP
