# CMake configuration folder

The structure of the project is reproduced in this folder to keep CMakeLists
files away from the source folders. It contains also some helper function
modules.

The project consists of header only libraries. In some IDEs, headers are not
displayed even using `target_sources` function as described at
<http://mariobadr.com/creating-a-header-only-library-with-cmake.html>: the files
are only displayed when an executable project is added to the ide, so a
[hack] using `add_custom_target` was also used to solve this problem.

[hack]: https://stackoverflow.com/questions/27039019/how-to-have-cmake-show-headers-that-are-not-part-of-any-binary-target-in-the-ide
