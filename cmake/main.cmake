
add_subdirectory (cmake/nowide nowide)
add_subdirectory (cmake/gui gui)

if (GUI_BUILD_EXAMPLES)
    include (examples)
endif ()
