/*
    Name:        samples/paint.cpp
    Purpose:     Paint sample application
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/03
    Licence:     Boost Software License, Version 1.0
*/
#include <gui/application.hpp>
#include <gui/gfx/context.hpp>
#include <gui/canvas.hpp>
#include <gui/window.hpp>

using namespace gui;

void on_paint( gfx::context<> &ctx )
{
    std::cout << "Drawing\n";
    ctx.set_line_width( 20 );
    ctx.set_source_rgba( 1, 0, 0, 0.1 );
    ctx.move_to( 10, 200 );
    ctx.line_to( 200, 10 );
    ctx.stroke();
}

int main()
{
    application<> app;
    window<>      mainWindow;
    canvas<>      mainCanvas( &mainWindow );

    mainWindow.add( &mainCanvas );
    mainWindow.set_title("Paint Example");
    mainWindow.set_size( 300, 180 );
    mainWindow.show_all();

    mainCanvas.sig_paint.connect( &on_paint );

    app.set_main_window( &mainWindow );
    app.run();

    return 0;
}
