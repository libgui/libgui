/*
    Name:        samples/minimal.cpp
    Purpose:     Minimal sample application
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/04/20
    Licence:     Boost Software License, Version 1.0
*/
#include <gui/application.hpp>
#include <gui/checkbox.hpp>
#include <gui/grid.hpp>
#include <gui/menu.hpp>
#include <gui/menuitem.hpp>
#include <gui/menubar.hpp>
#include <gui/messagedialog.hpp>
#include <gui/progressbar.hpp>
#include <gui/frame.hpp>
#include <gui/slider.hpp>
#include <gui/statusbar.hpp>
#include <gui/textbox.hpp>
#include <gui/window.hpp>

bool on_close( gui::toplevel<> *tlw )
{
    using namespace gui;

    messagedialog<>
    dlg( tlw, "Are you sure to quit?", "Question", id_yes|id_no, msg_question );

    if( dlg.show_modal() == id_yes )
        return true;

    return false;
}

int main()
{
    using namespace gui;

    application<> app;
    window<>      mainWindow;

    statusbar<> status( &mainWindow );
    status.set_text("Status bar");

    menubar<> mainMenu( &mainWindow );
    menu<>    menuFile;
    mainMenu.add_menu( &menuFile, "_File" );

    menu<>    menuRecent;
    menuFile.add_submenu( &menuRecent, "_Recent" );

    menuitem<> itemRecent("Re_cent File");
    menuRecent.add_item( &itemRecent );

    menuitem<> itemQuit("_Quit");
    menuFile.add_item( &itemQuit );

    textbox<> entry( &mainWindow );
    entry.set_text("Hello world");
    entry.expand( both_orientations );

    progressbar<> progress( &mainWindow );
    progress.set_max(100);
    progress.pulse();
    progress.set_position(50);

    slider<> slide( &mainWindow );
    slide.set_max(100);
    slide.set_position(50);

    checkbox<> btn( &mainWindow );
    checkbox<> btn2( &mainWindow );
    btn.set_label("_Exit");
    btn.set_image("network-wireless-medium");

    frame<> frm( &mainWindow, "Test" );

    grid<> mainGrid( &mainWindow );
    mainGrid.attach( mainMenu, 0, 0, 1, 1 );
    mainGrid.attach( entry, 0, 1, 1, 1 );
    mainGrid.attach( progress, 0, 2, 1, 1 );
    mainGrid.attach( slide, 0, 3, 1, 1 );
    mainGrid.attach( btn, 0, 4, 1, 1 );
    mainGrid.attach( frm, 0, 5, 1, 1 );
    mainGrid.attach( status, 0, 6, 1, 1 );

    mainWindow.add( &mainGrid );

    mainWindow.set_title("Minimal Example");
    mainWindow.set_size( 300, 180 );
    mainWindow.show_all();

    mainWindow.sig_close.connect( &on_close );

    btn.sig_select.connect( std::bind(&window<>::close, &mainWindow) );
    itemQuit.sig_select.connect( std::bind(&window<>::close, &mainWindow) );

    app.set_main_window( &mainWindow );
    app.run();

    return 0;
}
