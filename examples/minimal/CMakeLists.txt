project (minimal)

add_compile_options (-DGUI_NEED_GTK)
add_compile_options (-DGUI_HAVE_GTK)

find_package (PkgConfig REQUIRED)
pkg_check_modules (GTK3 REQUIRED gtk+-3.0)
include_directories (${GTK3_INCLUDE_DIRS})
link_directories (${GTK3_LIBRARY_DIRS})

add_executable (minimal minimal.cpp)
target_include_directories(minimal PUBLIC "${CMAKE_SOURCE_DIR}/include")
target_link_libraries (minimal ${GTK3_LIBRARIES})

#include (Utils)
#print_all_variables()
