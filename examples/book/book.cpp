/*
    Name:        samples/book.cpp
    Purpose:     Book controls sample application
    Author:      Andrea Zanellato
    Modified by: 
    Created:     2013/10/03
    Licence:     Boost Software License, Version 1.0
*/
#include <cstdio>
#include <vector>

#include <gui/application.hpp>
//#include <gui/grid.hpp>
#include <gui/notebook.hpp>
#include <gui/textbox.hpp>
#include <gui/window.hpp>

int main()
{
    using namespace gui;

    application<> app;

    window<>    mainWindow;
//  grid<>      mainGrid( &mainWindow );
    notebook<>  book( &mainWindow );

    textbox<> *entry;
    std::vector< textbox<> * > entries;

    int i;
    char buffer[32];

    for( i = 0; i < 10; ++i )
    {
        entry = new textbox<>( &book );
        sprintf( buffer, "Entry %d", i + 1 );
        entry->set_text( buffer );
        entries.push_back( entry );

        sprintf( buffer, "Page %d", i + 1 );
        book.append_page( entry, buffer );
    }

    book.expand( both_orientations );
    book.set_scrollable();
    book.set_selection( book.page_count() -1 );
//  mainGrid.attach( book, 0, 0, 1, 1 );

    mainWindow.add( &book );
    mainWindow.set_title("Book Example");
    mainWindow.set_size( 300, 180 );
    mainWindow.show_all();

    app.set_main_window( &mainWindow );
    app.run();

    entries.clear();

    return 0;
}
